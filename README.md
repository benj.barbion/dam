<p align="center">
    <img src="public/images/preview.png" alt="DAM" title="DAM" />
</p>

## Prérequis

PHP, ElasticSearch, une base de donnée.

## Etapes à suivre lors de la récupération du projet 

### ElasticSearch
1) #### Lancez ElasticSearch
2) #### Modifiez les variables du fichier .env à la racine du projet
   1. ELASTIC_HOST=
   2. ELASTIC_USERNAME=
   3. ELASTIC_PASSWORD=

### Base de données

1) #### Modifiez les variables du fichier .env à la racine du projet
   1. DB_CONNECTION=mysql (sqlite, pgsql, sqlsrv)
   2. DB_HOST=127.0.0.1
   3. DB_PORT=3306
   4. DB_DATABASE=
   5. DB_USERNAME=
   6. DB_PASSWORD=

### Pusher (avec Echo)

Pusher est utilisé dans le projet afin d'envoyer une **notification broadcasting** à l'utilisateur.

Lorsque le status d'un asset est modifié, l'utilisateur est directement notifié (sans même rafraichir la page).

1) #### Modifiez, au besoin, les variables du fichier .env à la racine du projet
   1. PUSHER_APP_ID=1648073
   2. PUSHER_APP_KEY=81883a5f16d440163205
   3. PUSHER_APP_SECRET=79d395945407704ad049
   4. PUSHER_HOST=

**ATTENTION ! Dans le cadre de ce projet, vous n'avez PAS besoin de modifier ces variables.**

Il n'est nécessaire de le faire que si vous liez l'application à un autre compte pusher.

### Etapes générales d'installation (dans une fenêtre terminal)

1) #### composer install
2) #### npm install
3) #### php artisan migrate:refresh --seed
4) #### php artisan icons:cache (très important pour la rapidité)
5) #### npm run dev
6) #### Dans un nouveau terminal lancez php artisan serve
7) #### Take a beer and enjoy !

**NB :** en cas de problème d'upload, vérifiez les paramètres de votre serveur PHP.

## Et les index ElasticSearch alors ? 

Ils sont automatiquement générés dans les seeders ! Vous n'avez rien à faire.

1. #### Artisan::call('scout:delete-index "App\Models\Asset"');
2. #### Artisan::call('scout:index "App\Models\Asset"');

## La barre de recherche générale

Cette barre de recherche peut être "magique", selon vos demandes.

**Quelques exemples de recherches possibles :**

1. fichier
2. dossier
3. image
4. video
5. application
6. pdf
7. html
8. ... recherche à l'infini.

## Les comptes utilisateurs (à travers leurs rôles)

Votre application est fournie avec une liste d'utilisateurs, possédant certains rôles (qui eux, contiennent des permissions).

### 1. Le héro, l'administrateur !
**Email :** benj.barbion@gmail.com<br />**Mot de passe :** Benjamin<br />

###### Liste des pouvoirs spéciaux :
1. files.*
2. folders.*
3. users.*
4. metadata.*

Il est le seul à pouvoir gérer les utilisateur ou encore à créer des dossiers...

### 2. Le contributeur confirmé
**Email :** adams@gmail.com<br />**Mot de passe :** Adams<br />

###### Liste des pouvoirs spéciaux :
1. files.edit
2. files.create
3. files.show
4. files.download
5. folders.show

### 3. Le contribteur
**Email :** rene@gmail.com<br />**Mot de passe :** René<br />

###### Liste des pouvoirs spéciaux :
1. files.create
2. files.show
3. files.download
4. folders.show

### 4. Bonus

Vous pouvez ajouter des utilisateur à travers le site et leurs mettre des rôles.

## Pour approfondir ses compétences Laravel...

https://laravel.com/docs/10.x/

https://laraveldaily.com/

https://laravel-news.com/

https://laracasts.com/

https://codecourse.com/

Et bien évidemment les chaines Youtube liées.
