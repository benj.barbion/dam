<?php

use App\Http\Controllers\FileController;
use App\Livewire\Admin\Assets;
use App\Livewire\Admin\Metadata;
use App\Livewire\Admin\Users;
use App\Livewire\Dashboard;
use App\Livewire\UserNotification;
use Illuminate\Support\Facades\Route;

Route::get('/', Dashboard::class)->name('dashboard');

Route::get('/files/{file}/download', [FileController::class, 'download'])->name('files.download');

Route::get('/files/{file}/serve', [FileController::class, 'serve'])->name('files.serve');

Route::get('/files/{file}/thumbnail', [FileController::class, 'thumbnail'])->name('files.thumbnail');

Route::get('/files/{folder}', Dashboard::class)->name('files.show');

Route::middleware('auth')->group(function () {
    Route::get('/notifications', UserNotification::class)->name('notifications');
    Route::get('/admin/users', Users::class)->name('admin.users')->middleware('can:users.update');
    Route::get('/admin/metadata', Metadata::class)->name('admin.metadata')->middleware('can:metadata.update');
    Route::get('/admin/assets', Assets::class)->name('admin.assets')->middleware('can:files.update');
});
