<?php

namespace Tests\Feature;

use App\Actions\CreateFolder;
use App\Enums\AssetStatus;
use App\Models\Folder;
use App\Models\MimeType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CreateFolderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_create_a_folder()
    {
        $user = User::factory()->create();

        $mime_type = MimeType::query()->firstWhere('name', 'folder');
        $parentFolder = Folder::make(['status' => AssetStatus::APPROVED, 'uuid' => Str::uuid(), 'name' => 'home']);
        $parentFolder->mimeType()->associate($mime_type);
        $parentFolder->save();

        $action = new CreateFolder();

        $folder = $action->handle('Test Folder', $parentFolder->uuid, $user);

        $this->assertNotNull($folder);
        $this->assertEquals('Test Folder', $folder->name);
        $this->assertEquals($parentFolder->id, $folder->parent_id);
        $this->assertEquals($user->id, $folder->user_id);
    }

    /** @test */
    public function it_throws_exception_if_parent_folder_not_found()
    {
        $user = User::factory()->create();

        $action = new CreateFolder();

        $this->expectException(ModelNotFoundException::class);
        $action->handle('Test Folder', 'invalid-uuid', $user);
    }

    /** @test */
    public function it_sets_pending_status_if_user_cannot_update_status()
    {
        $user = User::factory()->create();

        // Suppose que le $user ne peut pas mettre à jour le statut de n'importe quel dossier.
        $this->mock(User::class, function ($mock) use ($user) {
            $mock->shouldReceive('cannot')
                ->with('updateStatusAny', Folder::class)
                ->andReturn(true);
        });

        $mime_type = MimeType::query()->firstWhere('name', 'folder');
        $parentFolder = Folder::make(['status' => AssetStatus::APPROVED, 'uuid' => Str::uuid(), 'name' => 'home']);
        $parentFolder->mimeType()->associate($mime_type);
        $parentFolder->save();


        $action = new CreateFolder();

        $folder = $action->handle('Test Folder', $parentFolder->uuid, $user);

        $this->assertEquals(AssetStatus::PENDING, $folder->status);
    }

    // ... Ajoutez d'autres tests pour d'autres scénarios.
}
