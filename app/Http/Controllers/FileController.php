<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function serve(File $file)
    {
        return response()->file(Storage::path($file->real_path));
    }

    public function thumbnail(File $file)
    {
        return response()->file(Storage::path($file->thumbnail->path));
    }

    public function download(File $file)
    {
        return response()->download(Storage::path($file->real_path), $file->full_name);
    }
}
