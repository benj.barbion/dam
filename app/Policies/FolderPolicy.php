<?php

namespace App\Policies;

use App\Enums\AssetStatus;
use App\Models\Folder;
use App\Models\User;

class FolderPolicy
{
    /**
     * Determine if the user can view any folders.
     *
     * @param  User  $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * Determine if the user can view the folder.
     *
     * @param  User  $user
     * @param  Folder  $asset
     * @return bool
     */
    public function view(User $user, Folder $asset): bool
    {
        return $asset->status == AssetStatus::APPROVED || $asset->user->id == $user->id || $user->can('folders.update');
    }

    /**
     * Determine if the user can create folders.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can('folders.create');
    }

    /**
     * Determine if the user can update the folder.
     *
     * @param  User  $user
     * @param  Folder  $asset
     * @return bool
     */
    public function update(User $user, Folder $asset): bool
    {
        return $user->id == $asset->user?->id || $user->can('folders.update');
    }

    /**
     * Determine if the user can update the status of the folder.
     *
     * @param  User  $user
     * @return bool
     */
    public function updateStatus(User $user): bool
    {
        return $user->can('folders.update');
    }

    /**
     * Determine if the user can delete the folder.
     *
     * @param  User  $user
     * @param  Folder  $asset
     * @return bool
     */
    public function delete(User $user, Folder $asset): bool
    {
        return !$asset->is_root && ($user->id == $asset->user->id || $user->can('folders.delete'));
    }
}
