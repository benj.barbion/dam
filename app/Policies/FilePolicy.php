<?php

namespace App\Policies;

use App\Enums\AssetStatus;
use App\Models\File;
use App\Models\User;

class FilePolicy
{
    /**
     * Determine if the user can view any files.
     *
     * @param  User  $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * Determine if the user can view the file.
     *
     * @param  User  $user
     * @param  File  $asset
     * @return bool
     */
    public function view(User $user, File $asset): bool
    {
        return $asset->status == AssetStatus::APPROVED || $asset->user->id == $user->id || $user->can('files.update');
    }

    /**
     * Determine if the user can create files.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * Determine if the user can update the file.
     *
     * @param  User  $user
     * @param  File  $asset
     * @return bool
     */
    public function update(User $user, File $asset): bool
    {
        return $user->id == $asset->user->id || $user->can('files.update');
    }

    /**
     * Determine if the user can update the status of the file.
     *
     * @param  User  $user
     * @return bool
     */
    public function updateStatus(User $user): bool
    {
        return $user->can('files.update');
    }

    /**
     * Determine if the user can delete the file.
     *
     * @param  User  $user
     * @param  File  $asset
     * @return bool
     */
    public function delete(User $user, File $asset): bool
    {
        return $user->id == $asset->user->id || $user->can('files.delete');
    }
}
