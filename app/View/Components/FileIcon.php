<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class FileIcon extends Component
{
    private string $mimeType;

    /**
     * Create a new component instance.
     */
    public function __construct(string $mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * Determine the icon name to use based on the object's MIME type.
     *
     * This method maps a specific MIME type or a general category (like "image" or "video") to a representative icon.
     * If no matching MIME type is found, an "unknown" icon is returned.
     *
     * @return string The filename of the icon to use.
     */
    public function getIconName(): string
    {
        $simplifiedMimeType = $this->mimeType;

        if (Str::startsWith($simplifiedMimeType, 'image'))
            $simplifiedMimeType = 'image';

        if (Str::startsWith($simplifiedMimeType, 'video'))
            $simplifiedMimeType = 'video';

        $iconMappings = [
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'word-document-svgrepo-com.svg',
            'application/msword' => 'word-document-svgrepo-com.svg',
            'application/pdf' => 'pdf-document-svgrepo-com.svg',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'excel-document-svgrepo-com.svg',
            'image' => 'image-document-svgrepo-com.svg',
            'application/x-rar' => 'zip-document-svgrepo-com.svg',
            'text/html' => 'html-document-svgrepo-com.svg',
            'text/x-c' => 'html-document-svgrepo-com.svg',
            'text/plain' => 'txt-document-svgrepo-com.svg',
            'audio/mpeg' => 'audio-document-svgrepo-com.svg',
            'video' => 'video-document-svgrepo-com.svg',
            'folder' => 'folder-svgrepo-com.svg'
        ];

        return $iconMappings[$simplifiedMimeType] ?? 'unknown-document-svgrepo-com.svg';
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.files.file-icon');
    }
}
