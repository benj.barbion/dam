<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ContentType extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function definedMetadata() : BelongsToMany
    {
        return $this->belongsToMany(DefinedMetadata::class);
    }
}
