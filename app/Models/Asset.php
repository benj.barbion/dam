<?php

namespace App\Models;

use App\Enums\AssetStatus;
use App\Enums\MimeType;
use App\Traits\FlattensArray;
use App\Traits\ReadableSize;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Scout\Searchable;
use Parental\HasChildren;

class Asset extends Model
{
    use HasFactory, Searchable, ReadableSize, HasChildren, FlattensArray;

    protected $fillable = ['uuid', 'name', 'size', 'real_path', 'description', 'status'];
    protected $with = ['mimeType', 'user'];
    protected $perPage = 24;

    protected $casts = [
        'embedded_metadata' => 'array',
        'status' => AssetStatus::class
    ];

    protected static function booted(): void
    {
        //static::addGlobalScope('approved', fn (Builder $builder) => $builder->status(AssetStatus::APPROVED));
        static::addGlobalScope('sorted', fn (Builder $builder) => $builder->orderByDesc('created_at'));
    }

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    public function toSearchableArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'name' => $this->name,
            'type' => $this->asset_type,
            'content_type' => $this->content_type_name,
            'mime_type' => $this->mime_type_name,
            'owner' => $this->user_id,
            'path' => $this->real_path,
            'size' => $this->getOriginal('size'),
            'description' => $this->description,
            'tags' => $this->tags->pluck('name'),
            'status' => $this->status,
            'keywords' => $this->keywords->pluck('name'),
            'embedded_metadata' => $this->embedded_metadata ?? [],
            'custom_metadata' => $this->customMetadata?->datas ?? [],
            'defined_metadata' => $this->definedMetadataValues->pluck('pivot.value', 'name'),
            'created_at' => $this->created_at
        ];
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Folder::class, 'parent_id');
    }

    // Relation many to many with pivot
    public function definedMetadataValues(): BelongsToMany
    {
        return $this->belongsToMany(DefinedMetadata::class, 'asset_metadata')
            ->select(['defined_metadata.id', 'name', 'description', 'display_name'])
            ->withPivot('value');
    }


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->select(['id', 'name']);
    }

    public function mimeType(): BelongsTo
    {
        return $this->belongsTo(\App\Models\MimeType::class);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'asset_tag');
    }

    public function keywords(): BelongsToMany
    {
        return $this->belongsToMany(Keyword::class);
    }

    public function customMetadata(): HasOne
    {
        return $this->hasOne(CustomMetadata::class);
    }

    public function thumbnail(): HasOne
    {
        return $this->hasOne(Thumbnail::class);
    }

    public function size(): Attribute
    {
        return Attribute::make(get: fn (?int $value) => $this->readableSize($value));
    }

    public function flatEmbeddedMetadata(): Attribute
    {
        return Attribute::make(get: fn () => $this->flatten($this->embedded_metadata ?? []));
    }

    public function mimeTypeName(): Attribute
    {
        return Attribute::make(get: fn () => $this->mimeType?->name);
    }

    public function getFullNameAttribute(): string
    {
        return $this->name . '.' . MimeType::from($this->mime_type_name)->getExt();
    }

    public function getDefinedMetadataAttribute()
    {
        return $this->mimeType?->contentType?->definedMetadata;
    }

    public function getContentTypeNameAttribute()
    {
        return $this->mimeType?->contentType?->name;
    }

    public function scopeWithoutRoot(Builder $query): Builder
    {
        return $query->whereNotNull('parent_id');
    }

    public function scopeUuid(Builder $query, $uuid): Builder
    {
        return $query->where('uuid', $uuid);
    }

    public function scopeApproved(Builder $query): Builder
    {
        return $query->where('status', AssetStatus::APPROVED);
    }

    public function scopeStatus(Builder $query, AssetStatus $status): Builder
    {
        return $query->where('status', $status);
    }
}
