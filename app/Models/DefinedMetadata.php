<?php

namespace App\Models;

use App\Enums\InputType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class DefinedMetadata extends Model
{
    use HasFactory;

    protected $fillable = ['display_name', 'name', 'description', 'input'];

    protected $casts = [
        'input' => InputType::class
    ];

    public function contentTypes() : BelongsToMany
    {
        return $this->belongsToMany(ContentType::class);
    }
}
