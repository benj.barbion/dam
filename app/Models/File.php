<?php

namespace App\Models;

use Parental\HasParent;

class File extends Asset
{
    use HasParent;

    public function getAssetTypeAttribute(): string
    {
        return 'Fichier';
    }

    public function getIsFolderAttribute() : bool
    {
        return false;
    }
}
