<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Parental\HasParent;

class Folder extends Asset
{
    use HasParent;

    public function children() : HasMany
    {
        return $this->hasMany(Asset::class, 'parent_id')->with('mimeType:id,name');
    }

    public function foldersChildren(): HasMany
    {
        return $this->hasMany(Folder::class, 'parent_id')->with('mimeType:id,name');
    }

    public function filesChildren(): HasMany
    {
        return $this->hasMany(File::class, 'parent_id')->with('mimeType:id,name');
    }

    public function getAssetTypeAttribute(): string
    {
        return 'Dossier';
    }

    public function getIsFolderAttribute(): bool
    {
        return true;
    }

    public function scopeRoot(Builder $query): Builder
    {
        return $query->whereNull('parent_id');
    }

    public function getIsRootAttribute(): bool
    {
        return is_null($this->parent_id);
    }

    public function getIsEmptyAttribute(): bool
    {
        return $this->children->count() == 0;
    }
}
