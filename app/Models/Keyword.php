<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function assets()
    {
        return $this->belongsToMany(Asset::class);
    }

    public function scopePopular(Builder $query, int $total = 10): Builder
    {
        return $query->withCount('assets')->orderByDesc('assets_count')->take($total);
    }
}
