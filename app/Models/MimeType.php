<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MimeType extends Model
{
    use HasFactory;

    protected $fillable = ['name'];
    protected $with = ['contentType'];

    public function contentType() : BelongsTo
    {
        return $this->belongsTo(ContentType::class);
    }
}
