<?php

namespace App\Actions;

use App\Contracts\ICreateDefinedMetadata;
use App\Contracts\IValidator;
use App\Models\DefinedMetadata;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class CreateDefinedMetadata implements ICreateDefinedMetadata
{
    public function __construct(private readonly IValidator $validator)
    {
    }

    /**
     * @inheritDoc
     */
    public function handle(array $data): DefinedMetadata
    {
        $data = Validator::make($data, $this->validator->rules())->validate();

        $definedMetadata = DefinedMetadata::create(Arr::except($data, keys: 'content_types'));

        if ($content = Arr::get($data, 'content_types'))
            $definedMetadata->contentTypes()->sync($content);

        return $definedMetadata;
    }
}
