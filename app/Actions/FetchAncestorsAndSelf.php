<?php

namespace App\Actions;

use App\Contracts\Asset\IFetchAncestorsAndSelf;
use Illuminate\Support\Facades\DB;

class FetchAncestorsAndSelf implements IFetchAncestorsAndSelf
{
    /**
     * @inheritDoc
     */
    public function handle(int $assetId): string
    {
        $result = DB::select("
            with recursive laravel_cte as (
            select *, 0 as depth, cast(id as char(65535)) as path
            from assets
            where assets.id = :assetId
            union all
            select assets.*, depth - 1 as depth, concat(path, '.', assets.id)
            from assets
            inner join laravel_cte on laravel_cte.parent_id = assets.id
        )
        select name from laravel_cte", ['assetId' => $assetId]);

        return collect($result)->pluck('name')->reverse()->join('/');
    }
}
