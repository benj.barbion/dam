<?php

namespace App\Actions;

use App\Contracts\Asset\IUpdateAsset;
use App\Contracts\IValidator;
use App\Enums\AssetStatus;
use App\Models\Asset;
use App\Models\CustomMetadata;
use App\Models\Keyword;
use App\Models\Tag;
use App\Notifications\AssetStatusNotification;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class UpdateAsset implements IUpdateAsset
{
    public function __construct(private readonly IValidator $validator)
    {
    }

    /**
     * @inheritDoc
     */
    public function handle(Asset $asset, array $update): Asset
    {
        $validatedData = Validator::make($update, $this->validator->rules())->validate();

        if (Arr::exists($validatedData, 'name'))
            $asset->name = Arr::get($validatedData, 'name');

        if (Arr::exists($validatedData, 'description'))
            $asset->description = Arr::get($validatedData, 'description');

        if (Arr::exists($validatedData, 'status'))
            $this->updateStatus($asset, Arr::get($validatedData, 'status'));

        if (Arr::exists($validatedData, 'tags'))
            $this->updateTags($asset, Arr::get($validatedData, 'tags'));

        if (Arr::exists($validatedData, 'keywords'))
            $this->updateKeywords($asset, Arr::get($validatedData, 'keywords'));

        if (Arr::exists($validatedData, 'customMetadata'))
            $this->updateCustomMetadata($asset, Arr::get($validatedData, 'customMetadata'));

        if (Arr::exists($validatedData, 'definedMetadata'))
            $this->updateDefinedMetadata($asset, Arr::get($validatedData, 'definedMetadata'));

        $asset->save();

        return $asset;
    }

    private function updateCustomMetadata(Asset $asset, array $customMetadata): void
    {
        CustomMetadata::updateOrCreate(['asset_id' => $asset->id], ['datas' => array_filter($customMetadata)]);
    }

    private function updateStatus(Asset $asset, int $status): void
    {
        $status = AssetStatus::from($status);

        // Can be inside EVENT/LISTENERS
        if ($status != $asset->status)
        {
            $asset->status = $status;
            Notification::send($asset->user, new AssetStatusNotification($asset));
        }
    }

    private function updateKeywords(Asset $asset, array $keywords): void
    {
        if (empty($keywords))
        {
            $asset->keywords()->sync([]);
            return;
        }

        $names = collect($keywords)->map(fn($tag) => ['name' => $tag]);
        Keyword::upsert($names->all(), 'name');
        $asset->keywords()->sync(Keyword::whereIn('name', $keywords)->pluck('id'));
    }

    private function updateTags(Asset $asset, array $tags): void
    {
        if (empty($tags))
        {
            $asset->tags()->sync([]);
            return;
        }

        $names = collect($tags)->map(fn(string $tag) => ['name' => $tag]);
        Tag::upsert($names->all(), 'name');
        $asset->tags()->sync(Tag::whereIn('name', $tags)->pluck('id'));
    }

    private function updateDefinedMetadata(Asset $asset, array $definedMetadata): void
    {
        if (empty($definedMetadata))
        {
            $asset->definedMetadataValues()->sync([]);
            return;
        }

        $syncData = [];
        foreach ($definedMetadata as $metadataId => $value)
            if ($value !== null)
                $syncData[$metadataId] = ['value' => $value];

        $asset->definedMetadataValues()->sync($syncData);
    }
}
