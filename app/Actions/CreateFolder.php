<?php

namespace App\Actions;

use App\Contracts\Asset\ICreateFolder;
use App\Enums\AssetStatus;
use App\Models\Folder;
use App\Models\MimeType;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

class CreateFolder implements ICreateFolder
{
    /**
     * @inheritDoc
     */
    public function handle(string $folder, string $parent, User $owner): Folder
    {
        $parent = Folder::firstWhere('uuid', $parent);
        if ($parent == null)
            throw new ModelNotFoundException('Le parent n\'existe pas.');

        $mime = MimeType::firstOrCreate(['name' => \App\Enums\MimeType::Folder->value]);

        $asset = Folder::make(['name' => $folder]);

        $asset->uuid = Str::uuid();
        $asset->status = $owner->cannot('updateStatus', Folder::class) ? AssetStatus::PENDING : AssetStatus::APPROVED;

        $asset->mimeType()->associate($mime);
        $asset->user()->associate($owner);
        $asset->parent()->associate($parent);
        $asset->save();

        return $asset;
    }
}
