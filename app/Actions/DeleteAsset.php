<?php

namespace App\Actions;

use App\Contracts\Asset\IDeleteAsset;
use App\Models\Asset;

class DeleteAsset implements IDeleteAsset
{
    /**
     * @inheritDoc
     */
    public function handle(Asset $asset): bool|null
    {
        return $asset->delete();
    }
}
