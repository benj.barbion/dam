<?php

namespace App\Actions\User;

use App\Contracts\User\ICreateNewUser;
use App\Enums\Roles;
use App\Models\User;
use App\Validators\CreateUserValidator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements ICreateNewUser, CreatesNewUsers
{
    public function __construct(private readonly CreateUserValidator $validator)
    {
    }

    /**
     * @inheritDoc
     */
    public function create(array $input): User
    {
        Validator::make($input, $this->validator->rules())->validate();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        // Contributor by default
        $user->assignRole(Roles::CONTRIBUTOR);

        return $user;
    }
}
