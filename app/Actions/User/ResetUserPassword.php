<?php

namespace App\Actions\User;

use App\Contracts\User\IResetUserPassword;
use App\Models\User;
use App\Validators\PasswordValidationRules;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\ResetsUserPasswords;

class ResetUserPassword implements IResetUserPassword, ResetsUserPasswords
{
    use PasswordValidationRules;

    /**
     * @inheritDoc
     */
    public function reset(User $user, array $input): void
    {
        Validator::make($input, [
            'password' => $this->passwordRules(),
        ])->validate();

        $user->forceFill([
            'password' => Hash::make($input['password']),
        ])->save();
    }
}
