<?php

namespace App\Actions\User;

use App\Contracts\User\IMarkNotificationAsRead;
use App\Models\User;

class MarkNotificationAsRead implements IMarkNotificationAsRead
{
    /**
     * @inheritDoc
     */
    public function handle(User $user, string $notificationId): bool
    {
        $notification = $user->unreadNotifications->where('id', $notificationId)->firstOrFail();

        $notification->markAsRead();

        return true;
    }
}
