<?php

namespace App\Actions\User;

use App\Contracts\User\IDeleteUser;
use App\Models\User;
use Laravel\Jetstream\Contracts\DeletesUsers;

class DeleteUser implements IDeleteUser, DeletesUsers
{
    /**
     * @inheritDoc
     */
    public function delete(User $user): void
    {
        $user->deleteProfilePhoto();
        $user->tokens->each->delete();
        $user->assets->each->delete();
        $user->delete();
    }
}
