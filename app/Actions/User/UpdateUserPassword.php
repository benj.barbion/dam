<?php

namespace App\Actions\User;

use App\Contracts\IValidator;
use App\Contracts\User\IUpdateUserPassword;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\UpdatesUserPasswords;

class UpdateUserPassword implements IUpdateUserPassword, UpdatesUserPasswords
{
    public function __construct(private readonly IValidator $validator)
    {
    }

    /**
     * @inheritDoc
     */
    public function update(User $user, array $input): void
    {
        Validator::make($input, $this->validator->rules(), $this->validator->messages())
            ->validateWithBag('updatePassword');

        $user->forceFill([
            'password' => Hash::make($input['password']),
        ])->save();
    }
}
