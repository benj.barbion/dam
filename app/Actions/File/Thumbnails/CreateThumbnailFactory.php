<?php

namespace App\Actions\File\Thumbnails;

use App\Contracts\Asset\ICreateThumbnail;
use App\Enums\ContentType;
use App\Models\Asset;

class CreateThumbnailFactory
{
    public function make(Asset $asset): ?ICreateThumbnail
    {
        return match (ContentType::from($asset->content_type_name))
        {
            ContentType::Image => new CreateImageThumbnail(),
            default => null
        };
    }
}
