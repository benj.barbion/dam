<?php

namespace App\Actions\File\Thumbnails;

use App\Contracts\Asset\ICreateThumbnail;
use App\Models\File;
use App\Models\Thumbnail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CreateImageThumbnail implements ICreateThumbnail
{
    /**
     * @inheritdoc
     */
    public function handle(File $file): Thumbnail
    {
        $image = Image::make(Storage::path($file->real_path));

        $image->resize(null, 250, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $thumbnailName = uniqid() . '.' . $image->extension;

        $path = config('filesystems.upload_path') . '/thumbnails/' . $thumbnailName;

        Storage::put($path, (string) $image->encode());

        $thumbnail = Thumbnail::make(['path' => $path]);
        $thumbnail->asset()->associate($file);
        $thumbnail->save();

        return $thumbnail;
    }
}
