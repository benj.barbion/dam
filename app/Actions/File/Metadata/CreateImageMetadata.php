<?php

namespace App\Actions\File\Metadata;

use App\Contracts\Asset\ICreateMetadata;
use App\Models\File;
use Illuminate\Support\Facades\Storage;

class CreateImageMetadata implements ICreateMetadata
{
    /**
     * @inheritdoc
     */
    public function handle(File $file): File
    {
        $path = Storage::path($file->real_path);

        list($width, $height) = getimagesize($path);

        $embedded = [];

        $embedded['image'] = ['width' => $width, 'height' => $height];

        $sysMetadata = @exif_read_data($path);

        if (isset($sysMetadata['FileDateTime']))
            $embedded['image']['datetime'] = date('Y-m-d H:i:s', $sysMetadata['FileDateTime']);

        if (isset($sysMetadata['COMPUTED']['IsColor']))
            $embedded['image']['isColor'] = $sysMetadata['COMPUTED']['IsColor'];

        $file->embedded_metadata = $embedded;

        $file->save();

        return $file;
    }
}
