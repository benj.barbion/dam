<?php

namespace App\Actions\File\Metadata;

use App\Contracts\Asset\ICreateMetadata;
use App\Models\File;
use Illuminate\Support\Arr;
use Owenoj\LaravelGetId3\GetId3;

class CreateVideoMetadata implements ICreateMetadata
{
    /**
     * @inheritdoc
     */
    public function handle(File $file): File
    {
        $metas = GetId3::fromDiskAndPath(config('filesystems.default'), $file->real_path)->extractInfo();

        $embedded = [];

        if (Arr::exists($metas, 'audio'))
            $embedded['audio'] = Arr::only($metas['audio'], ['codec', 'bitrate', 'channelmode']);

        if (isset($metas['tags']['quicktime']['title']))
            $embedded['audio']['song']['title'] = Arr::first($metas['tags']['quicktime']['title']);

        if (isset($metas['tags']['quicktime']['artist']))
            $embedded['audio']['song']['artist'] = Arr::first($metas['tags']['quicktime']['artist']);

        if (isset($metas['tags']['quicktime']['album']))
            $embedded['audio']['song']['album'] = Arr::first($metas['tags']['quicktime']['album']);

        if (isset($metas['video']))
            $embedded['video'] = Arr::only($metas['video'], ['dataformat', 'frame_rate', 'resolution_x', 'resolution_y']);

        if (isset($metas['playtime_seconds']))
            $embedded['video']['durationInSeconds'] = $metas['playtime_seconds'];

        $file->embedded_metadata = $embedded;

        $file->save();

        return $file;
    }
}
