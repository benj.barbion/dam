<?php

namespace App\Actions\File\Metadata;

use App\Contracts\Asset\ICreateMetadata;
use App\Models\File;

class DefaultMetadata implements ICreateMetadata
{
    /**
     * @inheritdoc
     */
    public function handle(File $file): File
    {
        return $file;
    }
}
