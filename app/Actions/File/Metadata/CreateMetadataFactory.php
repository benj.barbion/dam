<?php

namespace App\Actions\File\Metadata;

use App\Contracts\Asset\ICreateMetadata;
use App\Enums\ContentType;
use App\Models\Asset;

class CreateMetadataFactory
{
    public function make(Asset $asset): ICreateMetadata
    {
        return match (ContentType::from($asset->content_type_name))
        {
            ContentType::Image => new CreateImageMetadata(),
            ContentType::Video => new CreateVideoMetadata(),
            ContentType::Audio => new CreateAudioMetadata(),
            default => new DefaultMetadata()
        };
    }
}
