<?php

namespace App\Actions\File\Metadata;

use App\Contracts\Asset\ICreateMetadata;
use App\Models\File;
use Illuminate\Support\Arr;
use Owenoj\LaravelGetId3\GetId3;

class CreateAudioMetadata implements ICreateMetadata
{
    /**
     * @inheritdoc
     */
    public function handle(File $file): File
    {
        $metas = GetId3::fromDiskAndPath(config('filesystems.default'), $file->real_path)->extractInfo();

        $embedded = [];

        if (Arr::exists($metas, 'audio'))
            $embedded['audio'] = Arr::only($metas['audio'], ['codec', 'dataformat', 'bitrate', 'channelmode', 'encoder_options']);

        $embedded['audio']['durationInSeconds'] = $metas['playtime_seconds'];

        if (isset($metas['tags']['id3v2']['album']))
            $embedded['audio']['song']['album'] = Arr::first($metas['tags']['id3v2']['album']);

        if (isset($metas['tags']['id3v2']['genre']))
            $embedded['audio']['song']['genre'] = Arr::first($metas['tags']['id3v2']['genre']);

        if (isset($metas['tags']['id3v2']['artist']))
            $embedded['audio']['song']['artist'] = Arr::first($metas['tags']['id3v2']['artist']);

        if (isset($metas['tags']['id3v2']['title']))
            $embedded['audio']['song']['title'] = Arr::first($metas['tags']['id3v2']['title']);

        if (isset($metas['tags']['id3v2']['year']))
            $embedded['audio']['song']['year'] = Arr::first($metas['tags']['id3v2']['year']);

        $file->embedded_metadata = $embedded;

        $file->save();

        return $file;
    }
}
