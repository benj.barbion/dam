<?php

namespace App\Actions\File;

use App\Contracts\Asset\IUpload;
use App\Models\File;
use Illuminate\Http\UploadedFile;

class HandleUpload implements IUpload
{
    /**
     * @inheritdoc
     */
    public function handle(UploadedFile $file): File
    {
        $originalFilePath = $file->storePublicly(config('filesystems.upload_path'));

        return File::make([
            'name' => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
            'size' => $file->getSize(),
            'real_path' => $originalFilePath
        ]);
    }
}
