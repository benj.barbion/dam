<?php

namespace App\Actions\File;

use App\Actions\File\Metadata\CreateMetadataFactory;
use App\Actions\File\Thumbnails\CreateThumbnailFactory;
use App\Contracts\Asset\ICreateFile;
use App\Contracts\Asset\IUpload;
use App\Contracts\IValidator;
use App\Enums\AssetStatus;
use App\Models\Asset;
use App\Models\File;
use App\Models\Folder;
use App\Models\MimeType;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CreateFile implements ICreateFile
{
    public function __construct(private readonly IUpload $upload,
                                private readonly IValidator $createFileValidator,
                                private readonly CreateMetadataFactory $createMetadataFactory,
                                private readonly CreateThumbnailFactory $createThumbnailFactory)
    {
    }

    /**
     * @inheritdoc
     */
    public function handle(UploadedFile $upload, string $parent, User $owner): Asset
    {
        $data = ['upload' => $upload, 'parent' => $parent, 'user' => $owner];

        Validator::make($data, $this->createFileValidator->rules())->validate();

        $parent = Folder::firstWhere('uuid', $parent);
        if ($parent === null)
            throw new ModelNotFoundException('Dossier parent non trouvé.');

        $mime = MimeType::firstOrCreate(['name' => $upload->getMimeType()]);

        $asset = $this->upload->handle($upload);

        $asset->uuid = Str::uuid();
        $asset->status = $owner->cannot('updateStatus', File::class) ? AssetStatus::PENDING : AssetStatus::APPROVED;

        $asset->mimeType()->associate($mime);
        $asset->user()->associate($owner);
        $asset->parent()->associate($parent);
        $asset->save();

        $this->createMetadataFactory->make($asset)->handle($asset);
        $this->createThumbnailFactory->make($asset)?->handle($asset);

        return $asset;
    }
}
