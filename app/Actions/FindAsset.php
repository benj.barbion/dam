<?php

namespace App\Actions;

use App\Contracts\Asset\IFindAsset;
use App\Contracts\IElasticSearchQueryBuilder;
use App\Models\Asset;
use App\Services\ElasticSearch\ElasticsearchQueryBuilder;
use Illuminate\Support\Arr;
use Laravel\Scout\Builder;

class FindAsset implements IFindAsset
{
    /**
     * @inheritDoc
     */
    public function handle(array $values): Builder
    {
        // We can also inject the dependency, if desired: private IElasticSearchQueryBuilder $elasticSearchQueryBuilder;
        $builder = ElasticsearchQueryBuilder::make();

        if ($value = Arr::get($values, 'global_meta'))
            $builder->addGlobalMetaQuery($value, ['embedded_metadata.*', 'custom_metadata.*', 'defined_metadata.*']);

        $this->addMatchQueries($builder, $values);
        $this->addShouldMatches($builder, $values);
        $this->addRangeQueries($builder, $values);

        if ($value = Arr::get($values, 'definedMetadata'))
            foreach ($value as $k => $v)
                $builder->addMatchQuery('defined_metadata.'.$k, $v);

        return Asset::search(Arr::get($values, 'global', ''))->options(['query' => $builder->build()]);
    }

    private function addMatchQueries(IElasticSearchQueryBuilder $builder, array $values)
    {
        $matches = [
            'owner' => 'owner',
            'name' => 'name',
            'description' => 'description',
            'status' => 'status',
            'type' => 'type',
            'audio.song.album' => 'embedded_metadata.audio.song.album',
            'audio.song.title' => 'embedded_metadata.audio.song.title',
            'audio.song.artist' => 'embedded_metadata.audio.song.artist',
            'audio.song.genre' => 'embedded_metadata.audio.song.genre',
        ];

        foreach ($matches as $key => $query) {
            if ($value = Arr::get($values, $key)) {
                $builder->addMatchQuery($query, $value);
            }
        }
    }

    private function addShouldMatches(IElasticSearchQueryBuilder $builder, array $values)
    {
        $shouldMatches = [
            'content_types' => 'content_type.keyword',
            'mime_types' => 'mime_type.keyword',
            'tags' => 'tags',
            'keywords' => 'keywords',
        ];

        foreach ($shouldMatches as $key => $query) {
            if ($value = Arr::get($values, $key)) {
                $builder->addShouldMatches($query, $value);
            }
        }
    }

    private function addRangeQueries(IElasticSearchQueryBuilder $builder, array $values)
    {
        $ranges = [
            'size' => 'size',
            'image.width' => 'embedded_metadata.image.width',
            'image.height' => 'embedded_metadata.image.height',
            'video.resolution_x' => 'embedded_metadata.video.resolution_x',
            'video.resolution_y' => 'embedded_metadata.video.resolution_y',
            'video.duration' => 'embedded_metadata.video.durationInSeconds',
            'audio.duration' => 'embedded_metadata.audio.durationInSeconds',
            'created_at' => 'created_at',
        ];

        foreach ($ranges as $key => $query) {
            if ($value = Arr::get($values, $key)) {
                $builder->addRangeQuery($query, $value);
            }
        }
    }
}
