<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Builder;

interface IElasticSearchEngine
{
    /**
     * Update the given model in the index.
     *
     * @param  Collection  $models
     * @return void
     */
    function update(Collection $models): void;

    /**
     * Remove the given model from the index.
     *
     * @param  Collection  $models
     * @return void
     */
    function delete(Collection $models): void;

    /**
     * Perform the given search on the engine.
     *
     * @param Builder $builder
     * @return mixed
     */
    function search(Builder $builder): mixed;

    /**
     * Perform the given search on the engine.
     *
     * @param Builder $builder
     * @param  int  $perPage
     * @param  int  $page
     * @return mixed
     */
    function paginate(Builder $builder, int $perPage, int $page): mixed;

    /**
     * Map the given results to instances of the given model.
     *
     * @param Builder $builder
     * @param  mixed  $results
     * @param Model $model
     * @return Collection
     */
    function map(Builder $builder, mixed $results, Model $model): Collection;

    /**
     * Get the total count from a raw result returned by the engine.
     *
     * @param  mixed  $results
     * @return int
     */
    function getTotalCount(mixed $results): int;

    /**
     * Flush all the model's records from the engine.
     *
     * @param  Model  $model
     * @return void
     */
    function flush(Model $model): void;

    /**
     * Create a search index.
     *
     * @param  string  $name
     * @param  array  $options
     * @return mixed
     */
    function createIndex(string $name, array $options = []): mixed;

    /**
     * Delete a search index.
     *
     * @param  string  $name
     * @return mixed
     */
    function deleteIndex(string $name): mixed;
}
