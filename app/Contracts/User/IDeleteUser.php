<?php

namespace App\Contracts\User;

use App\Models\User;

interface IDeleteUser
{
    /**
     * Deletes the provided user and cleans up associated resources.
     *
     * @param User $user The user instance to be deleted.
     * @return void
     */
    function delete(User $user): void;
}
