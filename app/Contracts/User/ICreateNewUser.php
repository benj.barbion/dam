<?php

namespace App\Contracts\User;

use App\Models\User;

interface ICreateNewUser
{
    /**
     * Processes the provided input data to create a new user in the system.
     *
     * @param array $input The array containing details needed for user creation (e.g., name, email, password).
     * @return User The newly created user instance.
     */
    function create(array $input): User;
}
