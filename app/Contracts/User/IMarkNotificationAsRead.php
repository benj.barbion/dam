<?php

namespace App\Contracts\User;

use App\Models\User;

interface IMarkNotificationAsRead
{
    /**
     * Marks the specified notification of the user as read.
     *
     * @param User $user The user instance containing the notification.
     * @param string $notificationId The unique ID of the notification to be marked as read.
     * @return bool Returns true upon successful operation.
     */
    function handle(User $user, string $notificationId): bool;
}
