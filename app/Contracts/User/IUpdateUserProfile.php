<?php

namespace App\Contracts\User;

use App\Models\User;

interface IUpdateUserProfile
{
    /**
     * Updates the given user's profile with the provided input data.
     *
     * @param User $user The user whose profile will be updated.
     * @param array $input The data to be updated in the user's profile.
     *
     * @return void
     */
    function update(User $user, array $input): void;
}
