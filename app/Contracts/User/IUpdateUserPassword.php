<?php

namespace App\Contracts\User;

use App\Models\User;

interface IUpdateUserPassword
{
    /**
     * Updates the specified user's password based on the provided input.
     *
     * @param User $user The user whose password is to be updated.
     * @param array $input An array containing the new password and any other required data.
     * @return void
     */
    function update(User $user, array $input): void;
}
