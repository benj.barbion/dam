<?php

namespace App\Contracts\User;

use App\Models\User;

interface IResetUserPassword
{
    /**
     * Resets the specified user's password based on provided input.
     *
     * @param User $user The user whose password is to be reset.
     * @param array $input An array containing the new password.
     * @return void
     */
    function reset(User $user, array $input): void;
}
