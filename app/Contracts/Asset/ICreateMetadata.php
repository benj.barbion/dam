<?php

namespace App\Contracts\Asset;

use App\Models\File;

interface ICreateMetadata
{
    /**
     * Processes the given File to extract or generate its metadata.
     *
     * @param File $file The file to process.
     * @return File The processed file with updated metadata.
     */
    function handle(File $file): File;
}
