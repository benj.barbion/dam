<?php

namespace App\Contracts\Asset;

use App\Models\File;
use Illuminate\Http\UploadedFile;

interface IUpload
{
    /**
     * Handle the uploaded file by storing it in the designated path and
     * returning a new File instance with its details.
     *
     * @param UploadedFile $file The file to be uploaded.
     * @return File A new File instance containing details about the uploaded file.
     */
    function handle(UploadedFile $file): File;
}
