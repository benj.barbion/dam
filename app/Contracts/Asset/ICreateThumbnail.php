<?php

namespace App\Contracts\Asset;

use App\Models\File;
use App\Models\Thumbnail;

interface ICreateThumbnail
{
    /**
     * Handle the process of generating a thumbnail for a given file.
     *
     * @param File $file The file for which the thumbnail needs to be created.
     * @return Thumbnail The generated thumbnail.
     */
    function handle(File $file): Thumbnail;
}
