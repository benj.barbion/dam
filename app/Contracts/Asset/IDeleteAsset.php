<?php

namespace App\Contracts\Asset;

use App\Models\Asset;

interface IDeleteAsset
{
    /**
     * Handles the deletion of a given asset.
     *
     * @param Asset $asset The asset to be deleted.
     *
     * @return bool|null Returns true if deletion was successful, false otherwise, or null if the model's delete method has no return value.
     *
     * @see FileObserver
     * @see FolderObserver
     */
    function handle(Asset $asset): bool|null;
}
