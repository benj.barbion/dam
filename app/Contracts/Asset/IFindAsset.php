<?php

namespace App\Contracts\Asset;

use Laravel\Scout\Builder;

interface IFindAsset
{
    /**
     * Handles the process of finding assets based on the given values.
     *
     * @param array $values The values used to filter and find assets.
     * @return Builder The result of the search query for assets.
     */
    function handle(array $values): Builder;
}
