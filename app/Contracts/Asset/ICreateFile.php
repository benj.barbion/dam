<?php

namespace App\Contracts\Asset;

use App\Models\Asset;
use App\Models\User;
use Illuminate\Http\UploadedFile;

interface ICreateFile
{
    /**
     * Handle the process of creating a new file record.
     *
     * Validates the provided data, associates necessary relations, saves the file record,
     * creates the associated metadata, and generates a thumbnail if necessary.
     *
     * @param UploadedFile $upload The uploaded file instance.
     * @param string $parent The UUID of the parent folder.
     * @param User $owner The user who owns the file.
     *
     * @return Asset The created file record.
     *
     * @throws \Illuminate\Validation\ValidationException If validation fails.
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If the parent folder is not found.
     */
    function handle(UploadedFile $upload, string $parent, User $owner): Asset;
}
