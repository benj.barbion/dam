<?php

namespace App\Contracts\Asset;

use App\Models\Asset;

interface IUpdateAsset
{
    /**
     * Update the provided asset with the given data.
     *
     * @param Asset $asset   Asset to update.
     * @param array $update  Data for updating the asset.
     *
     * @return Asset Updated asset.
     */
    function handle(Asset $asset, array $update): Asset;
}
