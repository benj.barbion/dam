<?php

namespace App\Contracts\Asset;

use App\Models\Folder;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface ICreateFolder
{
    /**
     * Handles the creation of a new folder.
     *
     * @param string $folder The name of the folder to create.
     * @param string $parent UUID of the parent folder where the new folder should be created.
     * @param User $owner The user who will own the folder.
     *
     * @return Folder Returns the instance of the created folder.
     *
     * @throws ModelNotFoundException If the provided parent folder doesn't exist in the database.
     */
    function handle(string $folder, string $parent, User $owner): Folder;
}
