<?php

namespace App\Contracts\Asset;

interface IFetchAncestorsAndSelf
{
    /**
     * Fetch the names of the asset's ancestors, including itself, using a recursive common table expression (CTE).
     *
     * @param int $assetId The ID of the asset for which the ancestors are to be fetched.
     * @return string A concatenated string of ancestor names and the asset itself, separated by '/'.
     */
    function handle(int $assetId): string;
}
