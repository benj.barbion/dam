<?php

namespace App\Contracts;

/**
 * Interface for Laravel validation.
 *
 * Provides a contract for setting up validation rules and messages.
 */
interface IValidator
{
    /**
     * Specifies the validation rules.
     *
     * @return array The validation rules.
     */
    function rules(): array;

    /**
     * Specifies the custom error messages for validation rules.
     *
     * @return array The custom messages for validation errors.
     */
    function messages(): array;
}
