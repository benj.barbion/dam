<?php

namespace App\Contracts;

interface IElasticSearchQueryBuilder
{
    /**
     * Adds a global metadata query for Elasticsearch, utilizing the "multi_match" feature.
     *
     * @param string $globalMeta The global metadata value to be matched.
     * @param array $fields The fields against which the global metadata value should be matched.
     * @return self Returns the current instance of the ElasticsearchQueryBuilder for fluent calls.
     */
    function addGlobalMetaQuery(string $globalMeta, array $fields): self;

    /**
     * Adds a range query for Elasticsearch to filter results within a given range.
     *
     * @param string $field The field on which the range filter should be applied.
     * @param array $range An associative array containing 'lte' (less than or equal to)
     *                     and/or 'gte' (greater than or equal to) values for range filtering.
     * @return self Returns the current instance of the ElasticsearchQueryBuilder for fluent calls.
     */
    function addRangeQuery(string $field, array $range): self;

    /**
     * Adds a match query for Elasticsearch to filter results based on exact matches.
     *
     * @param string $field The field on which the match filter should be applied.
     * @param string $value The value to match against the specified field.
     * @return self Returns the current instance of the ElasticsearchQueryBuilder for fluent calls.
     */
    function addMatchQuery(string $field, string $value): self;

    /**
     * Adds a conditional match query to the Elasticsearch query.
     * This allows for one or more values to potentially match a given field.
     *
     * @param string $field The field to match against.
     * @param array $values An array of values that should be matched against the field.
     * @param int $shouldMatch The minimum number of 'should' clauses that must match.
     * @return self Returns the current instance of the ElasticsearchQueryBuilder for fluent calls.
     */
    function addShouldMatches(string $field, array $values, int $shouldMatch = 1): self;

    /**
     * Builds and returns the constructed Elasticsearch query.
     *
     * @return array The constructed Elasticsearch query options.
     */
    function build(): array;
}
