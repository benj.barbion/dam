<?php

namespace App\Contracts;

use App\Models\DefinedMetadata;

interface ICreateDefinedMetadata
{
    /**
     * Creates a new instance of defined metadata based on the provided input data.
     *
     * @param array $data Data containing the details for the new defined metadata.
     * @return DefinedMetadata The newly created instance of defined metadata.
     */
    function handle(array $data): DefinedMetadata;
}
