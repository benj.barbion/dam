<?php

namespace App\Infolists\Components;

use Filament\Infolists\Components\Entry;

class KeyValue extends Entry
{
    protected string $view = 'infolists.components.key-value';
}
