<?php

namespace App\Validators;

use App\Contracts\IValidator;
use App\Enums\AssetStatus;
use Illuminate\Validation\Rules\Enum;

class UpdateAssetValidator implements IValidator
{
    use PasswordValidationRules;

    /**
     * @inheritDoc
     */
    function rules(): array
    {
        return [
            'name' => 'sometimes|required|min:2',
            'description' => 'sometimes|nullable',
            'status' => ['sometimes', new Enum(AssetStatus::class)],
            'tags' => 'sometimes|array',
            'tags.*' => 'string',
            'keywords' => 'sometimes|array',
            'keywords.*' => 'string',
            'customMetadata' => 'sometimes|array',
            'customMetadata.*' => 'string',
            'definedMetadata' => 'sometimes|array'
        ];
    }

    /**
     * @inheritDoc
     */
    function messages(): array
    {
        return [
        ];
    }
}
