<?php

namespace App\Validators;

use App\Contracts\IValidator;
use Laravel\Jetstream\Jetstream;

class CreateUserValidator implements IValidator
{
    use PasswordValidationRules;

    /**
     * @inheritDoc
     */
    function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ];
    }

    /**
     * @inheritDoc
     */
    function messages(): array
    {
        return [];
    }
}
