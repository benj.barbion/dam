<?php

namespace App\Validators;

use App\Contracts\IValidator;

class CreateFileValidator implements IValidator
{
    /**
     * @inheritDoc
     */
    function rules(): array
    {
        return [
            'parent' => 'required|min:1',
            'upload' => 'required|file|mimetypes:'.implode(',', \App\Enums\MimeType::values())
        ];
    }

    /**
     * @inheritDoc
     */
    function messages(): array
    {
        return [

        ];
    }
}
