<?php

namespace App\Validators;

use App\Contracts\IValidator;

class UpdatePasswordValidator implements IValidator
{
    use PasswordValidationRules;

    /**
     * @inheritDoc
     */
    function rules(): array
    {
        return [
            'current_password' => ['required', 'string', 'current_password:web'],
            'password' => $this->passwordRules(),
        ];
    }

    /**
     * @inheritDoc
     */
    function messages(): array
    {
        return [
            'current_password.current_password' => __('The provided password does not match your current password.'),
        ];
    }
}
