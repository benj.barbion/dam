<?php

namespace App\Validators;

use App\Contracts\IValidator;
use App\Enums\InputType;
use Illuminate\Validation\Rules\Enum;

class CreateDefinedMetadataValidator implements IValidator
{
    /**
     * @inheritDoc
     */
    function rules(): array
    {
        return [
            'display_name' => ['required', 'string', 'max:255'],
            'name' => ['required', 'alpha_dash', 'unique:defined_metadata'],
            'description' => ['sometimes', 'string'],
            'input' => [new Enum(InputType::class)],
            'content_types' => ['sometimes', 'array'],
            'content_types.*' => ['exists:content_types,id']
        ];
    }

    /**
     * @inheritDoc
     */
    function messages(): array
    {
        return [];
    }
}
