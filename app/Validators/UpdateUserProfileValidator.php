<?php

namespace App\Validators;

use App\Contracts\IValidator;
use App\Models\User;
use Illuminate\Validation\Rule;

class UpdateUserProfileValidator implements IValidator
{
    use PasswordValidationRules;

    public function __construct(private readonly User $user)
    {
    }

    /**
     * @inheritDoc
     */
    function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($this->user->id)],
            'photo' => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
        ];
    }

    /**
     * @inheritDoc
     */
    function messages(): array
    {
        return [];
    }
}
