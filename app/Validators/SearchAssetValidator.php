<?php

namespace App\Validators;

use App\Contracts\IValidator;
use App\Enums\AssetStatus;
use App\Enums\ContentType;
use App\Enums\MimeType;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class SearchAssetValidator implements IValidator
{
    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'global' => 'sometimes|string|max:255',
            'name' => 'sometimes|string|max:255',
            'description' => 'sometimes|string',
            'size.gte' => 'sometimes|numeric|min:0',
            'size.lte' => 'sometimes|numeric|min:0',
            'created_at.gte' => 'sometimes|date',
            'created_at.lte' => 'sometimes|date',
            'status' => ['nullable', new Enum(AssetStatus::class)],
            'owner' => ['sometimes', Rule::in([auth()->id()])],

            'audio.duration.gte' => 'sometimes|numeric|min:1',
            'audio.duration.lte' => 'sometimes|numeric|min:1',
            'audio.song.album' => 'sometimes|string',
            'audio.song.title' => 'sometimes|string',
            'audio.song.artist' => 'sometimes|string',
            'audio.song.genre' => 'sometimes|string',

            'video.duration.gte' => 'sometimes|numeric|min:1',
            'video.duration.lte' => 'sometimes|numeric|min:1',
            'video.resolution_x.gte' => 'sometimes|numeric|min:1',
            'video.resolution_x.lte' => 'sometimes|numeric|min:1',
            'video.resolution_y.gte' => 'sometimes|numeric|min:1',
            'video.resolution_y.lte' => 'sometimes|numeric|min:1',

            'image.width.gte' => 'sometimes|numeric|min:1',
            'image.width.lte' => 'sometimes|numeric|min:1',
            'image.height.gte' => 'sometimes|numeric|min:1',
            'image.height.lte' => 'sometimes|numeric|min:1',

            'global_meta' => 'sometimes|string|alpha',
            'content_types' => ['nullable', 'array'],
            'content_types.*' => [new Enum(ContentType::class)],
            'mime_types' => ['nullable', 'array'],
            'mime_types.*' => [new Enum(MimeType::class)],
            'tags' => 'sometimes|array',
            'tags.*' => 'sometimes|string|exists:tags,name',
            'keywords' => 'sometimes|array',
            'keywords.*' => 'sometimes|string|exists:keywords,name',
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages(): array
    {
        return [

        ];
    }
}
