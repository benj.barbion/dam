<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class FolderObserver
{
    /**
     * Handle the "deleting" event for the Folder model.
     * Deletes all child elements of the folder recursively.
     *
     * @param  Model  $model  The Folder model instance being deleted.
     * @return void
     */
    public function deleting(Model $model): void
    {
        $model->children->each->delete();
    }
}
