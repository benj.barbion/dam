<?php

namespace App\Observers;

use App\Models\File;
use Illuminate\Support\Facades\Storage;

class FileObserver
{
    /**
     * Handle the "deleting" event for the File model.
     * Deletes the physical file and its associated thumbnail, if any.
     *
     * @param  File  $model  The File model instance being deleted.
     * @return void
     */
    public function deleting(File $model): void
    {
        Storage::disk(config('filesystems.default'))->delete($model->real_path);

        if ($model->thumbnail)
            Storage::disk(config('filesystems.default'))->delete($model->thumbnail->path);
    }
}
