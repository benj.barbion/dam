<?php

namespace App\Observers;

use App\Models\ContentType;
use App\Models\MimeType as Model;
use Illuminate\Support\Str;

class MimeTypeObserver
{
    /**
     * Handle the "creating" event for the MimeType model.
     * Associates the MimeType with its corresponding ContentType.
     *
     * @param  Model  $model  The MimeType model instance being created.
     * @return void
     */
    public function creating(Model $model): void
    {
        $mimeTypeCategory = ContentType::query()->firstWhere('name', Str::before($model->name, '/'));

        $model->contentType()->associate($mimeTypeCategory);
    }
}
