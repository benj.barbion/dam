<?php

namespace App\Livewire;

use App\Contracts\User\IMarkNotificationAsRead;
use Livewire\Component;

class UserNotification extends Component
{
    protected $listeners = ['notificationRead' => '$refresh'];

    public function markAsRead(IMarkNotificationAsRead $asRead, string $notificationId): void
    {
        $asRead->handle(auth()->user(), $notificationId);

        $this->dispatch('notificationRead')->self();

        $this->dispatch('refresh-navigation-menu');
    }

    public function render()
    {
        return view('livewire.notifications')
            ->with('notifications', auth()->user()->unreadNotifications);
    }
}
