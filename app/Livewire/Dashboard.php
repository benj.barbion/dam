<?php

namespace App\Livewire;

use App\Models\Folder;
use App\Models\Keyword;
use App\Models\Tag;
use Livewire\Component;

class Dashboard extends Component
{
    public ?Folder $folder = null;

    protected $listeners = ['refresh-dashboard', '$refresh'];

    public function render(Folder $folderRepo, Tag $tagRepo, Keyword $keywordRepo)
    {
        return view('livewire.dashboard')
                ->with('root', $this->folder === null ? $folderRepo->root()->first() : $this->folder)
                ->with('tags', $tagRepo->popular(10)->get())
                ->with('keywords', $keywordRepo->popular(10)->get())
                ->with('assets_count', auth()->user()?->assets_count ?? 0);
    }
}
