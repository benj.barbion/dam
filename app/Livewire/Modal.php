<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Features\SupportEvents\On;

class Modal extends Component
{
    public ?string $view = null;
    public array $params = [];
    public bool $show = false;

    #[On('modal')]
    public function modal(string $component, ?array $params = null)
    {
        $this->view = $component;
        $this->params = $params ?? [];
        $this->show = true;
    }

    #[On('close-modal-live')]
    public function close()
    {
        $this->show = false;
        $this->view = null;
        $this->params = [];
    }

    public function render()
    {
        return view('livewire.modal');
    }
}
