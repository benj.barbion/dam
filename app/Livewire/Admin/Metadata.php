<?php

namespace App\Livewire\Admin;

use App\Enums\InputType;
use App\Models\DefinedMetadata;
use App\Traits\CanTriggerAlert;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Actions\CreateAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Table;
use Livewire\Component;
use Illuminate\Contracts\View\View;

class Metadata extends Component implements HasForms, HasTable
{
    use InteractsWithForms, InteractsWithTable, CanTriggerAlert;

    public function table(Table $table): Table
    {
        return $table
            ->query(DefinedMetadata::query())
            ->columns([
                TextColumn::make('display_name')->label('Label'),
                TextColumn::make('name')->label('Clé unique'),
                TextColumn::make('description')->label('Description')->default('—'),
                TextColumn::make('contentTypes.name'),
                TextColumn::make('input')->formatStateUsing(fn (InputType $state): string => $state->name)
            ])
            ->headerActions([
                CreateAction::make()
                    ->form(fn($form) => $this->form($form))
                    ->label('Créer une métadonnée')
                    ->successNotification(null)
                    ->after(fn() => $this->success())
            ])
            ->actions([
                ActionGroup::make([
                    EditAction::make()->form(fn (Form $form) => $this->form($form))
                        ->successNotification(null)
                        ->after(fn() => $this->success()),

                    DeleteAction::make()->requiresConfirmation()
                        ->action(fn (DefinedMetadata $record) => $record->delete())
                        ->successNotification(null)
                        ->after(fn() => $this->success())
                ])
            ])
            ->striped()
            ->defaultSort('id', 'desc');
    }

    public function form(Form $form)
    {
        return $form
            ->schema([
                TextInput::make('display_name')
                    ->hint('Nom qui sera affiché à l\'utilisateur')
                    ->label('Nom visible')
                    ->required()
                    ->string(),

                TextInput::make('name')
                    ->hint('Clé interne à cette métadonnée (alpha).')
                    ->label('Clé unique')
                    ->required()
                    ->alpha()
                    ->unique(ignoreRecord:true),

                Textarea::make('description')->string(),

                Select::make('contentTypes')
                    ->multiple()
                    ->relationship(name: 'contentTypes', titleAttribute: 'name')
                    ->preload()
                    ->required(),

                Select::make('input')
                    ->hint('Type de donnée demandée à l\'utilisateur')
                    ->options(InputType::class)
                    ->enum(InputType::class)
                    ->required()
            ]);
    }

    public function render(): View
    {
        return view('livewire.admin.metadatas');
    }
}
