<?php

namespace App\Livewire\Admin;

use App\Contracts\User\ICreateNewUser;
use App\Contracts\User\IDeleteUser;
use App\Models\User;
use App\Traits\CanTriggerAlert;
use App\Validators\PasswordValidationRules;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Actions\CreateAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Table;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Users extends Component implements HasForms, HasTable
{
    use InteractsWithForms, InteractsWithTable, PasswordValidationRules, CanTriggerAlert;

    public function table(Table $table): Table
    {
        return $table
            ->query(User::query())
            ->columns([
                TextColumn::make('name')->label('Nom')->searchable(),
                TextColumn::make('email')->label('Email')->searchable(),
                TextColumn::make('roles.name')->sortable()->searchable(),
                TextColumn::make('created_at')->dateTime()->sortable(),
                TextColumn::make('updated_at')->dateTime()->sortable(),
            ])
            ->actions([
                ActionGroup::make([
                    EditAction::make()
                        ->form(fn (Form $form) => $this->updateForm($form))
                        ->successNotification(null)
                        ->after(fn() => $this->success()),

                    DeleteAction::make()
                        ->requiresConfirmation()
                        ->action(fn (IDeleteUser $deleteUser, User $record) => $deleteUser->delete($record))
                        ->hidden(fn (User $record) => $record->id == auth()->id()),
                ])
            ])
            ->headerActions([
                CreateAction::make()
                    ->form(fn($form) => $this->form($form))
                    ->label('Créer un nouvel utilisateur')
                    ->using(fn (ICreateNewUser $createNewUser, array $data) => $createNewUser->create($data))
                    ->successNotification(null)
                    ->after(fn() => $this->success())
            ])
            ->striped()
            ->defaultSort('id', 'desc');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name')->required()->autofocus()->alpha(),
                TextInput::make('email')->required()->email()->unique(ignoreRecord:true),
                TextInput::make('password')->required()->rules($this->passwordRules())->password(),
                TextInput::make('password_confirmation')->required()->password(),
            ]);
    }

    public function updateForm(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name')->required()->autofocus()->alpha(),
                TextInput::make('email')->required()->email()->unique(ignoreRecord:true),
                Select::make('roles')->multiple()->relationship(name: 'roles', titleAttribute: 'name')->preload()
            ]);
    }

    public function render(): View
    {
        return view('livewire.admin.users');
    }
}
