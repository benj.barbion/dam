<?php

namespace App\Livewire\Admin;

use App\Contracts\Asset\IDeleteAsset;
use App\Contracts\Asset\IFindAsset;
use App\Enums\MimeType;
use App\Models\Asset;
use App\Traits\CanTriggerAlert;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Tables;
use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Actions\BulkAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Illuminate\Contracts\View\View;
use Livewire\Features\SupportEvents\On;

class Assets extends Component implements HasForms, HasTable
{
    use InteractsWithForms;
    use InteractsWithTable;
    use CanTriggerAlert;

    private IFindAsset $findAsset;

    public ?array $search = null;

    protected $listeners = ['asset-updated', '$refresh'];

    public function boot(IFindAsset $findAsset)
    {
        $this->findAsset = $findAsset;
    }

    #[On('search-user')]
    public function searchUser(array $data)
    {
        if (count($data) > 0)
            $this->search = $data;
    }

    protected function applySearchToTableQuery(Builder $query): Builder
    {
        if ($this->search !== null)
            $query->whereIn('id', $this->findAsset->handle($this->search)->keys());
        return $query;
    }

    public function table(Table $table): Table
    {
        return $table
            ->query(Asset::withoutRoot())
            ->columns([
                Tables\Columns\TextColumn::make('')->label('Type')->default(fn(Asset $record) => MimeType::from($record->mimeType->name)->getExt()),
                Tables\Columns\TextColumn::make('user.name_unique'),
                Tables\Columns\TextColumn::make('name')->wrap(),
                Tables\Columns\TextColumn::make('size')->numeric()->sortable()->default('—'),
                Tables\Columns\TextColumn::make('status')->badge(),
                Tables\Columns\TextColumn::make('updated_at')->dateTime()->sortable(),
            ])
            ->actions([
                ActionGroup::make([
                    Tables\Actions\Action::make('Voir le fichier')
                        ->icon('heroicon-m-magnifying-glass')
                        ->action(fn (Asset $record) => $this->dispatch('modal', component: 'file.preview', params: ['uuid' => $record->uuid]))
                        ->hidden(fn (Asset $record) => $record->is_folder),

                    Tables\Actions\Action::make('Voir les détails')
                        ->icon('heroicon-m-information-circle')
                        ->action(fn (Asset $record) => $this->dispatch('modal', component: 'asset.show', params: ['uuid' => $record->uuid])),

                    EditAction::make()
                        ->action(fn (Asset $record) => $this->dispatch('modal', component: 'asset.update', params : ['uuid' => $record->uuid]))
                        ->hidden(fn (Asset $record) => Gate::denies('update', $record)),

                    DeleteAction::make()->requiresConfirmation()
                        ->action(fn (IDeleteAsset $deleteAsset, Asset $record) => $deleteAsset->handle($record))
                        ->hidden(fn (Asset $record) => Gate::denies('delete', $record)),
                ])
            ])
            ->groupedBulkActions([
                BulkAction::make('supprimer')
                    ->requiresConfirmation()
                    ->action(fn (IDeleteAsset $deleteAsset, Collection $records) => $records->each(fn (Asset $record) => $deleteAsset->handle($record))),
            ])
            ->checkIfRecordIsSelectableUsing(fn (Asset $record): bool => Gate::allows('delete', $record))
            ->striped()
            ->defaultSort('id', 'desc');
    }

    public function render(): View
    {
        return view('livewire.admin.assets');
    }
}
