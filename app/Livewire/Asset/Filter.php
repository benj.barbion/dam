<?php

namespace App\Livewire\Asset;

use App\Enums\AssetStatus;
use App\Enums\MimeType;
use App\Enums\ContentType;
use App\Models\DefinedMetadata;
use App\Models\File;
use App\Models\Keyword;
use App\Models\Tag;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class Filter extends Component implements HasForms
{
    use InteractsWithForms;

    public array $data = [];

    public function mount(): void
    {
        //$this->data = session('form_data', []);
        $this->form->fill($this->data);
    }

    public function save(): void
    {
        abort_unless(auth()->check(), 403);

        $data = Arr::filter_recursive($this->form->getState());

        session(['form_data' => $data]);

        $this->dispatch('search-user', data: $data);
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('File')
                    ->tabs([
                        Tabs\Tab::make('general')->label('Général')->schema($this->generalTab())->columns(),
                        Tabs\Tab::make('metadata')->label('Métadonnées')->schema($this->metadataTab()),
                        Tabs\Tab::make('Prédéfinies')->schema($this->definedMeta()),
                        Tabs\Tab::make('Image')->schema($this->imageTab()),
                        Tabs\Tab::make('Vidéo')->schema($this->videoTab()),
                        Tabs\Tab::make('Audio')->schema($this->audioTab()),
                    ]),
            ])
            ->statePath('data');
    }

    private function generalTab(): array
    {
        return [
            TextInput::make('global')->label('Recherche globale')->hint('Recherche dans toutes les données disponibles.')->columnSpanFull()->string(),
            TextInput::make('name')->label('Nom')->string(),
            TextInput::make('description')->string(),

            Fieldset::make('Taille du fichier')->schema([
                TextInput::make('size.gte')->prefix('De')->suffix('KB')->hiddenLabel()->numeric(),
                TextInput::make('size.lte')->prefix('à')->suffix('KB')->hiddenLabel()->numeric()
            ]),

            Fieldset::make('Date de publication')->schema([
                DatePicker::make('created_at.gte')->prefix('Du')->hiddenLabel()->date(),
                DatePicker::make('created_at.lte')->prefix('au')->hiddenLabel()->date()
            ]),

            Select::make('status')->hint('Seulement pour les membres ayant la permission.')->placeholder('Sélectionner un statut')->columnSpanFull()
                ->options(AssetStatus::class)
                ->enum(AssetStatus::class)
                ->hidden(Gate::denies('updateStatus', File::class))
        ];
    }

    private function audioTab(): array
    {
        return [
            Fieldset::make('Durée de l\'audio (exclus les vidéos)')->schema([
                TextInput::make('audio.duration.gte')->hiddenLabel()->prefix('De')->suffix('sec.')->numeric()->minValue(1),
                TextInput::make('audio.duration.lte')->hiddenLabel()->prefix('à')->suffix('sec.')->numeric()->minValue(1)
            ]),

            Fieldset::make('Détail du son (cherche également les vidéos)')->schema([
                TextInput::make('audio.song.album')->label('Album')->string(),
                TextInput::make('audio.song.title')->label('Titre')->string(),
                TextInput::make('audio.song.artist')->label('Artiste')->string(),
                TextInput::make('audio.song.genre')->label('Genre')->string(),
            ]),
        ];
    }

    private function definedMeta(): array
    {
        return
            DefinedMetadata::query()->with('contentTypes')->get()
            ->map(function (DefinedMetadata $meta)
            {
                return TextInput::make('definedMetadata.'.$meta->name)
                    ->label($meta->display_name)
                    ->type($meta->input->name)
                    ->hint('Pour : '.$meta->contentTypes->pluck('name')->join(', '). '.');
            })->toArray();
    }

    private function videoTab(): array
    {
        return [
            Fieldset::make('Durée de la vidéo')->schema([
                TextInput::make('video.duration.gte')->hiddenLabel()->prefix('De')->suffix('sec.')->numeric()->minValue(1),
                TextInput::make('video.duration.lte')->hiddenLabel()->prefix('à')->suffix('sec.')->numeric()->minValue(1)
            ]),

            Fieldset::make('Résolution x')->schema([
                TextInput::make('video.resolution_x.gte')->hiddenLabel()->prefix('De')->suffix('px')->numeric()->minValue(1),
                TextInput::make('video.resolution_x.lte')->hiddenLabel()->prefix('à')->suffix('px')->numeric()->minValue(1)
            ]),

            Fieldset::make('Résolution y')->schema([
                TextInput::make('video.resolution_y.gte')->hiddenLabel()->prefix('De')->suffix('px')->numeric()->minValue(1),
                TextInput::make('video.resolution_y.lte')->hiddenLabel()->prefix('à')->suffix('px')->numeric()->minValue(1)
            ]),
        ];
    }

    private function imageTab(): array
    {
        return [
            Fieldset::make('Width')->schema([
                TextInput::make('image.width.gte')->hiddenLabel()->prefix('De')->suffix('px')->numeric()->minValue(1),
                TextInput::make('image.width.lte')->hiddenLabel()->prefix('à')->suffix('px')->numeric()->minValue(1)
            ]),

            Fieldset::make('Height')->schema([
                TextInput::make('image.height.gte')->hiddenLabel()->prefix('De')->suffix('px')->numeric()->minValue(1),
                TextInput::make('image.height.lte')->hiddenLabel()->prefix('à')->suffix('px')->numeric()->minValue(1)
            ])
        ];
    }

    private function metadataTab(): array
    {
        return [
            TextInput::make('global_meta')->alpha()->label('Recherche globale')->hint('Recherche dans les métadonnées utilisateur et système.'),

            Select::make('content_types')
                ->label('Content Type')
                ->placeholder('Sélectionner un content type')
                ->hint('L\'asset devra remplir au moins une sélection.')
                ->multiple()
                ->options(ContentType::class)
                ->rules(['nullable', 'array', 'in:' . implode(',', ContentType::values())]),

            Select::make('mime_types')
                ->multiple()
                ->label('Type de donnée')
                ->options(MimeType::class)
                ->placeholder('Sélectionner un type')
                ->hint('L\'asset devra remplir au moins une sélection.')
                ->rules(['nullable', 'array', 'in:' . implode(',', MimeType::values())]),

            TagsInput::make('tags')
                ->suggestions(Tag::distinct()->pluck('name')->all())
                ->placeholder('Choisir les tags')
                ->hint('L\'asset devra remplir au moins une sélection.'),

            TagsInput::make('keywords')
                ->suggestions(Keyword::distinct()->pluck('name')->all())
                ->label('Mots-clés')
                ->placeholder('Choisir les mots-clés')
                ->hint('L\'asset devra remplir au moins une sélection.'),
        ];
    }

    public function resetForm()
    {
        $this->data = [];

        $this->form->fill($this->data);

        session(['form_data' => $this->data]);
    }

    public function render()
    {
        return view('livewire.asset.filter');
    }
}
