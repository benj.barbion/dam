<?php

namespace App\Livewire\Asset;

use App\Contracts\Asset\IDeleteAsset;
use App\Contracts\Asset\IFindAsset;
use App\Enums\AssetStatus;
use App\Models\Asset;
use App\Models\File;
use App\Models\Folder;
use App\Validators\SearchAssetValidator;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Support\Facades\Validator;
use Livewire\Attributes\Locked;
use Livewire\Component;
use Livewire\Features\SupportEvents\On;
use Livewire\WithPagination;

class Browsing extends Component
{
    use WithPagination;

    public Folder $root;

    #[Locked] public ?array $search = null;

    protected $listeners = ['refresh' => '$refresh'];

    #[On('modify-root')]
    public function modify(Folder $root): void
    {
        $this->root = $root;
        $this->search = null;
        $this->resetPage();
    }

    #[On('search-user')]
    public function search(SearchAssetValidator $validator, array $data): void
    {
        Validator::validate($data, $validator->rules());

        if (isset($data['status']))
            $this->authorize('updateStatus', File::class);

        else if (empty($data['owner']))
            $data['status'] = AssetStatus::APPROVED->value;

        $this->resetPage();

        $this->search = $data;
    }

    /**
     * @throws AuthorizationException
     */
    #[On('delete-asset')]
    public function delete(IDeleteAsset $deleteAsset, Asset $asset): void
    {
        $this->authorize('delete', $asset);

        $deleteAsset->handle($asset);
    }

    public function resetFilter(): void
    {
        $this->search = null;
    }

    public function render(IFindAsset $findAsset, Asset $assetRepo)
    {
        if ($this->search !== null)
        {
            $assets = $findAsset->handle($this->search)->paginate();
            $assets->load('thumbnail');

            return view('livewire.asset.browsing')
                ->with('files', $assets)
                ->with('assets_count', $assetRepo->approved()->count());
        }

        return view('livewire.asset.browsing')
            ->with('files', $this->root->children()->approved()->with(['thumbnail'])->paginate())
            ->with('assets_count', $assetRepo->approved()->count());
    }
}
