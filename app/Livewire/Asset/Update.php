<?php

namespace App\Livewire\Asset;

use App\Actions\UpdateAsset;
use App\Enums\AssetStatus;
use App\Models\Asset;
use App\Models\DefinedMetadata;
use App\Models\Keyword;
use App\Models\Tag;
use App\Traits\CanTriggerAlert;
use App\Traits\FlattensArray;
use Filament\Forms\Components\KeyValue;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rules\Enum;
use Livewire\Component;

class Update extends Component implements HasForms
{
    use InteractsWithForms, FlattensArray, CanTriggerAlert;

    public Asset $asset;

    public array $data = [];

    public function mount(string $uuid): void
    {
        $this->asset = Asset::uuid($uuid)->firstOrFail();

        $this->setData();
    }

    /**
     * @throws AuthorizationException
     */
    public function save(UpdateAsset $update): bool
    {
        $data = $this->form->getState();

        $this->authorize('update', $this->asset);

        if (Gate::denies('updateStatus', $this->asset))
            $data['status'] = AssetStatus::PENDING->value;

        $update->handle($this->asset, $data);

        $this->success();
        $this->dispatch(event: 'refresh')->to(Browsing::class);
        $this->dispatch(event: 'asset-updated');

        return true;
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Tabs::make('Update')->tabs([
                Tabs\Tab::make('Général')->schema($this->generalTab())->columns(),
                Tabs\Tab::make('Métadonnées')->schema($this->metadataTab()),
            ])
        ])->statePath('data');
    }

    private function metadataTab():array
    {
        $custom = KeyValue::make('customMetadata')
            ->label('Métadonnées personnalisées')
            ->keyPlaceholder('Insérer un nom')->valuePlaceholder('Insérer une valeur')
            ->keyLabel('Nom')->valueLabel('Valeur')
            ->hint('Valeurs définies par l\'utilisateur.')
            ->addActionLabel('Ajouter une métadonnée');

        $defined = $this->asset->definedMetadata?->map(function (DefinedMetadata $meta)
        {
            return TextInput::make('definedMetadata.'.$meta->id)
                ->label($meta->display_name)
                ->type($meta->input->name)
                ->hint($meta->description)
                ->visible(Gate::allows('update', $this->asset));
        })->toArray();

        return array_merge([$custom], $defined ?? []);
    }

    private function generalTab(): array
    {
        return [
            TextInput::make('name')->label('Nom')->columnSpanFull()->rules('required|min:2'),

            Textarea::make('description')->columnSpanFull(),

            TagsInput::make('tags')->suggestions(Tag::distinct()->pluck('name')->all())->placeholder('Entrez les tags...')->columnSpanFull(),

            TagsInput::make('keywords')->suggestions(Keyword::distinct()->pluck('name')->all())->label('Mots-clés')->columnSpanFull()
                ->placeholder('Entrez les mots-clés...'),

            Select::make('status')
                ->options(AssetStatus::class)
                ->placeholder('Sélectionner un statut')
                ->columnSpanFull()
                ->rules([new Enum(AssetStatus::class)])
                ->hidden(Gate::denies('updateStatus', $this->asset)),
        ];
    }

    private function setData(): void
    {
        $this->data = [
            'name' => $this->asset->name,
            'description' => $this->asset->description,
            'tags' => $this->asset->tags->pluck('name'),
            'keywords' => $this->asset->keywords?->pluck('name'),
            'customMetadata' => $this->asset->customMetadata?->datas,
            'sysMetadata' => $this->flatten($this->asset->embedded_metadata ?? []),
            'status' => $this->asset->status?->value ?? 1,
            'mime' => $this->asset->mime_type_name,
            'content' => $this->asset->content_type_name,
            'created_at' => $this->asset->created_at->diffForHumans(),
            'updated_at' => $this->asset->updated_at->diffForHumans(),
            'size' => $this->asset->size,
            'definedMetadata' => $this->asset->definedMetadataValues->pluck('pivot.value', 'id')->all()
        ];

        $this->form->fill($this->data);
    }

    public function render()
    {
        return view('livewire.asset.update');
    }
}
