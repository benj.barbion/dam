<?php

namespace App\Livewire\Asset;

use App\Actions\CreateFolder;
use App\Actions\File\CreateFile;
use App\Livewire\Dashboard;
use App\Models\File;
use App\Models\Folder;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\ValidationException;
use Livewire\Attributes\Locked;
use Livewire\Component;
use Livewire\WithFileUploads;

class Upload extends Component
{
    use WithFileUploads;

    #[Locked] public string $uuid = '';

    public string $name = '';
    public $upload;

    /**
     * @throws AuthorizationException
     */
    public function createFolder(CreateFolder $createFolder): void
    {
        $this->resetErrorBag();

        $this->authorize('create', Folder::class);

        $createFolder->handle($this->name, $this->uuid, auth()->user());

        $this->name = '';

        $this->endOfUpload();
    }

    /**
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function updatedUpload(CreateFile $createFile, UploadedFile $file): void
    {
        $this->authorize('create', File::class);

        $createFile->handle($file, $this->uuid, auth()->user());
    }

    public function endOfUpload(): void
    {
        //$this->dispatch('refresh-dashboard')->to(Dashboard::class);
        $this->dispatch('refresh')->to(Browsing::class);
    }

    public function render()
    {
        return view('livewire.asset.upload');
    }
}
