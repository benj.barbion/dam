<?php

namespace App\Livewire\Asset;

use App\Contracts\Asset\IFetchAncestorsAndSelf;
use App\Enums\AssetStatus;
use App\Enums\MimeType;
use App\Models\Asset;
use App\Traits\FlattensArray;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Infolists\Components\Tabs;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Components\ViewEntry;
use Filament\Infolists\Concerns\InteractsWithInfolists;
use Filament\Infolists\Contracts\HasInfolists;
use Filament\Infolists\Infolist;
use Livewire\Component;

class Show extends Component implements HasForms, HasInfolists
{
    use InteractsWithInfolists, InteractsWithForms, FlattensArray;

    private Asset $asset;

    public function mount(string $uuid)
    {
        $this->asset = Asset::uuid($uuid)->firstOrFail();

        if (auth()->check())
            $this->authorize('view', $this->asset);
        else
            abort_unless($this->asset->status == AssetStatus::APPROVED, 403);
    }

    public function assetInfoList(Infolist $infolist): Infolist
    {
        return $infolist
            ->record($this->asset)
            ->schema([
                Tabs::make('Label')
                    ->tabs([
                        Tabs\Tab::make('Données générales')->schema($this->generalTab())->columns(3),
                        Tabs\Tab::make('Les métadonnées')->schema($this->metadataTab())->hidden($this->noneMeta())
                    ])
            ]);
    }

    private function metadataTab(): array
    {
        return [
            ViewEntry::make('flat_embedded_metadata')
                ->label('Métadonnées système')
                ->view('infolists.components.key-value')
                ->hidden(count($this->asset->embedded_metadata ?? []) == 0),

            ViewEntry::make('customMetadata.datas')
                ->label('Métadonnées utilisateur')
                ->view('infolists.components.key-value')
                ->hidden(is_null($this->asset->customMetadata) || count($this->asset->customMetadata->datas) == 0),

            ViewEntry::make('')
                ->label('Métadonnées prédéfinies')
                ->default($this->asset->definedMetadataValues->pluck('pivot.value', 'display_name')->all())
                ->view('infolists.components.key-value')
                ->hidden($this->asset->definedMetadataValues->isEmpty()),
        ];
    }

    private function noneMeta(): bool
    {
        return count($this->asset->embedded_metadata ?? []) == 0 &&
            (is_null($this->asset->customMetadata) || count($this->asset->customMetadata->datas) == 0) &&
                $this->asset->definedMetadataValues->isEmpty();
    }

    private function generalTab(): array
    {
        return [
            TextEntry::make('ancestors_and_self')->label('Chemin complet')->columnSpanFull()
                ->default(fn (IFetchAncestorsAndSelf $ancestorsAndSelf) => $ancestorsAndSelf->handle($this->asset->id)),
            TextEntry::make('name')->label('Nom')->columnSpanFull(),
            TextEntry::make('')->label('Type fichier')->badge()->default(MimeType::from($this->asset->mimeType->name)->getExt()),
            TextEntry::make('size')->label('Taille du fichier')->badge()->default('—'),
            TextEntry::make('created_at')->label('Création')->since()->badge(),
            TextEntry::make('description')->columnSpanFull()->default('Aucune description'),
            TextEntry::make('tags.name')->label('Tags')->badge()->hidden($this->asset->tags->isEmpty())->columnSpanFull(),
            TextEntry::make('keywords.name')->label('Mots-clés')->badge()->hidden($this->asset->keywords->isEmpty())->columnSpanFull(),
        ];
    }

    public function render()
    {
        return view('livewire.asset.show');
    }
}
