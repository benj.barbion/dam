<?php

namespace App\Livewire\Folder;

use App\Models\Folder;
use Illuminate\Support\Collection;
use Livewire\Component;
use Livewire\Features\SupportEvents\On;

class Tree extends Component
{
    public Folder $folder;
    public Collection $children;
    public bool $isOpen = false;

    public function mount(Folder $folder, bool $isOpen = false): void
    {
        $this->children = collect();
        $this->folder = $folder;
        $this->isOpen = $isOpen;

        if ($isOpen)
            $this->loadChildren();
    }

    #[On('modify-folder.{folder.uuid}')]
    public function modify()
    {
        $this->loadChildren();
        $this->isOpen = true;
    }

    public function loadChildren(): void
    {
        if ($this->children->isEmpty())
            $this->children = $this->folder->foldersChildren()->get(['id', 'uuid', 'name']);
    }

    public function render()
    {
        return view('livewire.folder.tree');
    }
}
