<?php

namespace App\Livewire\File;

use App\Enums\AssetStatus;
use App\Enums\ContentType;
use App\Models\Asset;
use App\Models\File;
use Livewire\Component;

class Preview extends Component
{
    public File $asset;

    public function mount($uuid)
    {
        $this->asset = Asset::uuid($uuid)->firstOrFail();

        if (auth()->check())
            $this->authorize('view', $this->asset);
        else
            abort_unless($this->asset->status == AssetStatus::APPROVED, 403);
    }

    public function getComponent(): ?string
    {
        $contentType = $this->asset->content_type_name;

        $componentMap = [
            ContentType::Image->value => 'files.preview.image-preview',
            ContentType::Text->value => 'files.preview.document-preview',
            ContentType::Video->value => 'files.preview.video-preview',
            ContentType::Audio->value => 'files.preview.audio-preview',
        ];

        if (isset($componentMap[$contentType]))
            return $componentMap[$contentType];

        if ($contentType == ContentType::Application->value && in_array($this->asset->mime_type_name, ['application/pdf', 'application/json']))
            return 'files.preview.document-preview';

        return null;
    }

    public function render()
    {
        return view('livewire.file.preview');
    }
}
