<?php

namespace App\Services\ElasticSearch;

use App\Contracts\IElasticSearchEngine;
use Elastic\Elasticsearch\Client;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Scout\Builder;
use Laravel\Scout\Engines\Engine;

class ElasticSearchEngine extends Engine implements IElasticSearchEngine
{
    public function __construct(protected Client $client)
    {
    }

    /**
     * @inheritdoc
     */
    public function update($models): void
    {
        $models->each(function($model)
        {
            $params = [
                'index' => $model->searchableAs(),
                'type' => $model->searchableAs(),
                'id' => $model->id,
                'body' => $model->toSearchableArray()
            ];

            $this->client->index($params);
        });
    }

    /**
     * @inheritdoc
     */
    public function delete($models): void
    {
        $models->each(function($model)
        {
            $params = [
                'index' => $model->searchableAs(),
                'type' => $model->searchableAs(),
                'id' => $model->id
            ];

            $this->client->delete($params);
        });
    }

    /**
     * @inheritdoc
     */
    public function search(Builder $builder): array
    {
        return $this->mergedSearch($builder);
    }

    /**
     * @inheritdoc
     */
    public function paginate(Builder $builder, $perPage, $page): array
    {
        return $this->mergedSearch($builder, [
            'from' => ($page - 1) * $perPage,
            'size' => 20
        ]);
    }

    private function mergedSearch(Builder $builder, array $options = [])
    {
        if (Str::length($builder->query) > 0)
            $builder->options['query']['bool']['must'][] = ['multi_match' => ['query' => $builder->query]];

        $params = array_merge_recursive([
            'index' => $builder->model->searchableAs(),
            'type' => $builder->model->searchableAs(),
            'body' => [
                'from' => Arr::get($options, 'from', 0),
                'size' => Arr::get($options, 'size', 20),
                'query' => $builder->options['query'] ?? [],
                'sort' => $builder->options['sort'] ?? []
            ],
        ], $options);

        if (empty($params['body']['query']))
            $params['body']['query'] = ['match_all' => new \stdClass()];

        return $this->client->search($params)->asArray();
    }

    /**
     * @inheritdoc
     */
    public function mapIds($results): array|\Illuminate\Support\Collection
    {
        return collect(Arr::get($results, 'hits.hits'))->pluck('_id')->all();
    }

    /**
     * @inheritdoc
     */
    public function map(Builder $builder, mixed $results, $model): \Illuminate\Database\Eloquent\Collection
    {
        $hits = Arr::get($results, 'hits.hits');

        if (count($hits) === 0)
            return $model->newCollection();

        return $model->getScoutModelsByIds($builder, collect($hits)->pluck('_id')->all());
    }

    /**
     * @inheritdoc
     */
    public function getTotalCount(mixed $results): int
    {
        return Arr::get($results, 'hits.total.value', 0);
    }

    /**
     * @inheritdoc
     */
    public function flush($model): void
    {
        $this->deleteIndex($model->searchableAs());
        $this->createIndex($model->searchableAs());
    }

    /**
     * @inheritdoc
     */
    public function createIndex($name, array $options = []): mixed
    {
        $default_analyzer = [
            'type' => 'custom',
            'tokenizer' => 'standard',
            'filter' => ['lowercase', 'asciifolding']
        ];

        try {
            $this->client->indices()->create([
                'index' => $name,
                'body' => [
                    'settings' => [
                        'analysis' => [
                            'analyzer' => [
                                $options['analyzer'] ?? $default_analyzer
                            ]
                        ]
                    ]
                ]
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteIndex($name): mixed
    {
        try {
            $this->client->indices()->delete([
                'index' => $name
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function lazyMap(Builder $builder, $results, $model)
    {
        // TODO: Implement lazyMap() method.
    }
}
