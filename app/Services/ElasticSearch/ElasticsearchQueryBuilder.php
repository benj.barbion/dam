<?php

namespace App\Services\ElasticSearch;

use App\Contracts\IElasticSearchQueryBuilder;

class ElasticsearchQueryBuilder implements IElasticSearchQueryBuilder
{
    private array $options = [];

    /**
     * Creates and returns a new instance of the ElasticsearchQueryBuilder.
     *
     * @return ElasticsearchQueryBuilder
     */
    public static function make(): ElasticsearchQueryBuilder
    {
        return new self();
    }

    /**
     * {@inheritdoc}
     */
    public function addGlobalMetaQuery(string $globalMeta, array $fields): self
    {
        $this->options['bool']['must'][] = [
            'multi_match' => [
                'query' => $globalMeta,
                'fields' => $fields,
                'lenient' => true
            ]
        ];

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addRangeQuery(string $field, array $range): self
    {
        $rangeQuery = [];

        if (!empty($range['lte']))
            $rangeQuery['lte'] = $range['lte'];

        if (!empty($range['gte']))
            $rangeQuery['gte'] = $range['gte'];

        $this->options['bool']['must'][] = ['range' => [$field => $rangeQuery]];

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addMatchQuery(string $field, string $value): self
    {
        $this->options['bool']['must'][] = ['match' => [$field => $value]];

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addShouldMatches(string $field, array $values, int $shouldMatch = 1): self
    {
        $shouldMatches = [];

        foreach ($values as $value)
            $shouldMatches[] = ['match' => [$field => $value]];

        $this->options['bool']['must'][] = ['bool' => ['should' => $shouldMatches, 'minimum_should_match' => $shouldMatch]];

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function build(): array
    {
        return $this->options;
    }
}
