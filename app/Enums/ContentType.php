<?php

namespace App\Enums;

enum ContentType: string
{
    case Folder = 'folder';
    case Application = 'application';
    case Audio = 'audio';
    case Image = 'image';
    case Text = 'text';
    case Video = 'video';

    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
