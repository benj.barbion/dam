<?php

namespace App\Enums;

use Filament\Support\Contracts\HasColor;
use Filament\Support\Contracts\HasIcon;
use Filament\Support\Contracts\HasLabel;

enum AssetStatus: int implements HasIcon, HasColor, HasLabel
{
    case PENDING = 1;
    case APPROVED = 2;
    case RESTRICTED = 3;

    public function isApproved(): bool
    {
        return $this == self::APPROVED;
    }

    public function getIcon(): ?string
    {
        return match ($this) {
            self::PENDING => 'heroicon-m-eye',
            self::APPROVED => 'heroicon-m-check',
            self::RESTRICTED => 'heroicon-m-x-mark',
        };
    }

    public function getColor(): ?string
    {
        return match ($this) {
            self::PENDING => 'warning',
            self::APPROVED => 'success',
            self::RESTRICTED => 'danger',
        };
    }

    public function getLabel(): ?string
    {
        return match ($this) {
            self::PENDING => 'Asset en attente',
            self::APPROVED => 'Asset validé',
            self::RESTRICTED => 'Asset rejeté',
        };
    }
}
