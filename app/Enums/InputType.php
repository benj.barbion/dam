<?php

namespace App\Enums;

enum InputType: int
{
    case TEXT = 1;
    case DATE = 2;
    case COLOR = 3;
    case EMAIL = 4;
    case NUMBER = 5;
    case URL = 6;
}
