<?php

namespace App\Enums;

enum Roles: string
{
    case CONTRIBUTOR = 'Contributeur';
    case CONFIRMED_CONTRIBUTOR = 'Contributeur confirmé';
    case ADMINISTRATOR = 'Administrateur';
}
