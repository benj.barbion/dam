<?php

namespace App\Traits;

trait FlattensArray
{
    /**
     * Flatten a multi-dimensional array into a single level with dot notation keys.
     *
     * @param  array  $array
     * @param  string $prefix
     * @return array
     */
    public function flatten(array $array, string $prefix = ''): array
    {
        $result = [];
        foreach($array as $key => $value)
        {
            if (is_array($value))
                $result += $this->flatten($value, $prefix . $key . '.');
            else
                $result[$prefix . $key] = $value;
        }
        return $result;
    }
}
