<?php

namespace App\Traits;

trait ReadableSize
{
    /**
     * Convert bytes to a human readable size format.
     *
     * @param  ?float $bytes
     * @return ?string
     */
    public function readableSize(?float $bytes): ?string
    {
        if (is_null($bytes))
            return null;

        $units = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

        for ($i = 0; $bytes > 1024; $i++)
            $bytes /= 1024;

        return round($bytes, 2) . ' ' . $units[$i];
    }
}
