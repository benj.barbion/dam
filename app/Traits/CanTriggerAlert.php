<?php

namespace App\Traits;

trait CanTriggerAlert
{
    /**
     * Success notification.
     *
     * @param  string  $message
     * @return void
     */
    protected function success(string $message = 'L\'opération est un succès !')
    {
        $this->dispatch('swal:alert', title: $message, icon: 'success');
    }

    /**
     * Danger notification.
     *
     * @param  string  $message
     * @return void
     */
    protected function danger(string $message = 'Aie aie aie...')
    {
        $this->dispatch('swal:alert', title: $message, icon: 'error');
    }
}
