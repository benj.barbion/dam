<?php

namespace App\Providers;

use App\Models\File;
use App\Models\Folder;
use App\Models\MimeType;
use App\Observers\FileObserver;
use App\Observers\FolderObserver;
use App\Observers\MimeTypeObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        File::observe(FileObserver::class);
        Folder::observe(FolderObserver::class);
        MimeType::observe(MimeTypeObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
