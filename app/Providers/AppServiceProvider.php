<?php

namespace App\Providers;

use App\Actions\CreateDefinedMetadata;
use App\Actions\CreateFolder;
use App\Actions\DeleteAsset;
use App\Actions\FetchAncestorsAndSelf;
use App\Actions\File\CreateFile;
use App\Actions\File\HandleUpload;
use App\Actions\File\Thumbnails\CreateImageThumbnail;
use App\Actions\FindAsset;
use App\Actions\UpdateAsset;
use App\Actions\User\CreateNewUser;
use App\Actions\User\DeleteUser;
use App\Actions\User\MarkNotificationAsRead;
use App\Actions\User\UpdateUserPassword;
use App\Contracts\Asset\ICreateFile;
use App\Contracts\Asset\ICreateFolder;
use App\Contracts\Asset\ICreateThumbnail;
use App\Contracts\Asset\IDeleteAsset;
use App\Contracts\Asset\IFetchAncestorsAndSelf;
use App\Contracts\Asset\IFindAsset;
use App\Contracts\Asset\IUpdateAsset;
use App\Contracts\Asset\IUpload;
use App\Contracts\IElasticSearchQueryBuilder;
use App\Contracts\IValidator;
use App\Contracts\User\ICreateNewUser;
use App\Contracts\User\IDeleteUser;
use App\Contracts\User\IMarkNotificationAsRead;
use App\Services\ElasticSearch\ElasticSearchEngine;
use App\Services\ElasticSearch\ElasticsearchQueryBuilder;
use App\Validators\CreateDefinedMetadataValidator;
use App\Validators\CreateFileValidator;
use App\Validators\CreateUserValidator;
use App\Validators\UpdateAssetValidator;
use App\Validators\UpdatePasswordValidator;
use Elastic\Elasticsearch\ClientBuilder;
use Filament\Support\Colors\Color;
use Filament\Support\Facades\FilamentColor;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Laravel\Scout\EngineManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(IFindAsset::class, FindAsset::class);
        $this->app->bind(IElasticSearchQueryBuilder::class, ElasticsearchQueryBuilder::class);
        $this->app->bind(IMarkNotificationAsRead::class, MarkNotificationAsRead::class);
        $this->app->bind(IDeleteAsset::class, DeleteAsset::class);
        $this->app->bind(ICreateFolder::class, CreateFolder::class);
        $this->app->bind(ICreateFile::class, CreateFile::class);
        $this->app->bind(IUpdateAsset::class, UpdateAsset::class);
        $this->app->bind(IDeleteUser::class, DeleteUser::class);
        $this->app->bind(ICreateNewUser::class, CreateNewUser::class);
        $this->app->bind(ICreateThumbnail::class, CreateImageThumbnail::class);
        $this->app->bind(IUpload::class, HandleUpload::class);
        $this->app->bind(IFetchAncestorsAndSelf::class, FetchAncestorsAndSelf::class);

        $this->app->when(CreateFile::class)->needs(IValidator::class)->give(CreateFileValidator::class);
        $this->app->when(CreateNewUser::class)->needs(IValidator::class)->give(CreateUserValidator::class);
        $this->app->when(UpdateUserPassword::class)->needs(IValidator::class)->give(UpdatePasswordValidator::class);
        $this->app->when(CreateDefinedMetadata::class)->needs(IValidator::class)->give(CreateDefinedMetadataValidator::class);
        $this->app->when(UpdateAsset::class)->needs(IValidator::class)->give(UpdateAssetValidator::class);

        \Livewire\Component::macro('emit', fn ($event) => $this->dispatch($event));
        \Livewire\Component::macro('dispatchBrowserEvent', fn ($event) => $this->dispatch($event));

        Arr::macro('filter_recursive', function ($array) {
            return array_filter(array_map(function ($value) {
                return is_array($value) ? Arr::filter_recursive($value) : $value;
            }, $array));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->singleton('elasticsearch', function()
        {
            return ClientBuilder::create()
                ->setHosts([config('elasticsearch.host')])
                ->setSSLVerification(false)
                ->setBasicAuthentication(config('elasticsearch.username'), config('elasticsearch.password'))
                ->build();
        });

        resolve(EngineManager::class)->extend('elasticsearch', fn() => new ElasticSearchEngine(app('elasticsearch')));

        FilamentColor::register(['primary' => Color::Blue,]);
    }
}
