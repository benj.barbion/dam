<?php

return [
    'host' => env('ELASTIC_HOST', 'https://localhost:9200'),
    'username' => env('ELASTIC_USERNAME'),
    'password' => env('ELASTIC_PASSWORD'),
];
