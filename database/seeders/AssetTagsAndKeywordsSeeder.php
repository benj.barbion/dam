<?php

namespace Database\Seeders;

use App\Contracts\Asset\IUpdateAsset;
use App\Models\Asset;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class AssetTagsAndKeywordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(IUpdateAsset $updateAsset)
    {
        $assets = Asset::all();

        $possibleTags = ['important', 'final', 'sauvegarde', 'testons', 'modèle', 'copie', 'référence'];
        $possibleKeywords = ['projet', 'finance', 'équipe', 'réunion', 'analyse', 'stratégie', 'marketing', 'ventes', 'développement', 'recherche'];

        foreach ($assets as $asset) {
            $data = [
                'tags' => Arr::random($possibleTags, rand(1, 3)),
                'keywords' => Arr::random($possibleKeywords, rand(1, 3))
            ];

            $updateAsset->handle($asset, $data);
        }
    }
}
