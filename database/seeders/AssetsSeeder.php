<?php

namespace Database\Seeders;

use App\Contracts\Asset\ICreateFile;
use App\Contracts\Asset\ICreateFolder;
use App\Enums\AssetStatus;
use App\Models\Asset;
use App\Models\Folder;
use App\Models\MimeType;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws ValidationException
     */
    public function run(ICreateFolder $createFolder, ICreateFile $createFile): void
    {
        Artisan::call('scout:delete-index "App\Models\Asset"');
        Artisan::call('scout:index "App\Models\Asset"');

        $disk = Storage::disk(config('filesystems.default'));
        if ($disk->exists(config('filesystems.upload_path')))
            $disk->deleteDirectory(config('filesystems.upload_path'));

        $root = $this->rootFolder();

        $folders = ['pidv', 'vacances', 'nourriture', 'soleil', 'misc', 'sport', 'histoire'];
        foreach (array_reverse($folders) as $folderName)
            $createFolder->handle(folder: $folderName, parent: $root->uuid, owner: User::where('email', 'benj.barbion@gmail.com')->first());

        $directoryPath = database_path('seeders/files');
        $files = glob($directoryPath . '/*.*');

        foreach ($files as $path)
        {
            $uploadedFile = new UploadedFile($path, basename($path), mime_content_type($path), null, true);
            $createFile->handle($uploadedFile, $root->uuid, User::inRandomOrder()->first());
        }

        $assets = Asset::withoutGlobalScope('approved')->get();
        foreach ($assets as $asset)
        {
            $asset->created_at = Carbon::now()->addSeconds(rand(0, 60));
            $asset->status = AssetStatus::APPROVED;
            $asset->save();
        }
    }

    private function rootFolder(): Folder
    {
        $mime_type = MimeType::query()->firstWhere('name', 'folder');
        $asset = Folder::make(['status' => AssetStatus::APPROVED, 'uuid' => Str::uuid(), 'name' => 'home']);
        $asset->mimeType()->associate($mime_type);
        $asset->save();
        return $asset;
    }
}
