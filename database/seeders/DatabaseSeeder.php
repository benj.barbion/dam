<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(RolesAndPermissions::class);
        $this->call(UserSeeder::class);
        $this->call(ContentTypeAndMimeTypeSeeder::class);
        $this->call(AssetsSeeder::class);
        $this->call(DefinedMetadataSeeder::class);
        $this->call(AssetTagsAndKeywordsSeeder::class);
    }
}
