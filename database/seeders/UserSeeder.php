<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->createUser(name: 'Benjamin', email: 'benj.barbion@gmail.com', role: 'Administrateur');
        $this->createUser(name: 'Adams', email: 'adams@gmail.com', role: 'Contributeur confirmé');
        $this->createUser(name: 'René', email: 'rene@gmail.com', role: 'Contributeur');
    }

    private function createUser($name, $email, $role)
    {
        $user = User::factory()->create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($name)
        ]);

        $user->assignRole($role);

        $user->save();
    }
}
