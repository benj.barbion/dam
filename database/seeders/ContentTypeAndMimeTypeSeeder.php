<?php

namespace Database\Seeders;

use App\Enums\ContentType;
use App\Enums\MimeType;
use Illuminate\Database\Seeder;

class ContentTypeAndMimeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach (MimeType::values() as $mime)
        {
            $content = explode('/', $mime)[0];

            if (in_array($content, ContentType::values()))
            {
                $contentEntry = \App\Models\ContentType::query()->firstOrCreate(['name' => $content]);

                $mimeEntry = \App\Models\MimeType::query()->firstOrCreate(['name' => $mime]);
                $mimeEntry->contentType()->associate($contentEntry);

                $mimeEntry->save();
            }
        }
    }
}
