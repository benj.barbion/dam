<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Create permissions
        $permissions = [
            'files.*', 'files.create', 'files.delete', 'files.show', 'files.download', 'files.update',
            'folders.*', 'folders.create', 'folders.delete', 'folders.show', 'folders.update',
            'users.*', 'users.create', 'users.update', 'users.delete', 'users.show',
            'metadata.*', 'metadata.create', 'metadata.update', 'metadata.delete', 'metadata.show'
        ];

        foreach ($permissions as $permission)
            Permission::create(['name' => $permission]);

        // Create roles
        $adminRole = Role::create(['name' => 'Administrateur']);
        $adminRole->givePermissionTo(permissions: [
            'files.*', 'folders.*', 'users.*', 'metadata.*'
        ]);

        $confirmedContributorRole = Role::create(['name' => 'Contributeur confirmé']);
        $confirmedContributorRole->givePermissionTo(permissions: [
            'files.create', 'files.update', 'files.show', 'files.download', 'folders.show'
        ]);

        $contributorRole = Role::create(['name' => 'Contributeur']);
        $contributorRole->givePermissionTo(permissions: [
            'files.show', 'files.download', 'files.create', 'folders.show'
        ]);
    }
}
