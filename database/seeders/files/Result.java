import course.*;
import identity.esa.*;
import identity.Gender;
import java.time.LocalDate;

public class Result {
    public static void main(String[] args) {
        LocalDate birthday = LocalDate.of(1985,1,1);

        //declare professor
        Professor prof1 = new Professor("Bla", "Bli", "34, rue des blabla", Gender.MALE, birthday, "ESA2020-jkj45");
        Professor prof2 = new Professor("Blu", "Bli", "34, rue des blabla", Gender.MALE, birthday.minusDays(3), "ESA2020-jkfds45");
        Professor prof3 = new Professor("Blo", "Bli", "34, rue des blabla", Gender.FEMALE, birthday.plusWeeks(33), "ESA2020-jk33j45");

        //declare student
        Student student1 = new Student("Bler", "Bli", "34, rue des blabla", Gender.MALE, birthday.minusYears(9), "ESA2020-11");
        Student student2 = new Student("Blal", "Bli", "34, rue des blabla", Gender.UNKNOWN, birthday.minusYears(2), "ESA2020-jkkdjfdj45");
        Student student3 = new Student("Blakdd", "Bli", "34, rue des blabla", Gender.MALE, birthday.minusYears(1), "ESA2020-22");

        //method 1 declaration course
        Course math = new Course("Mathématiques", prof1);
        Course science = new Course("Science", prof3);

        //method 2 declaration course
        Course french = new Course("Français");
        french.setProfessor(prof2);

        //no professor
        Course gym = new Course("Gymnastique");

        //add course with reference to professor (now we can see all courses for one specific professor)
        prof1.addCourse(math);
        prof2.addCourse(french);
        prof3.addCourse(science);

        //cannot add Mathématiques once again
        Course math2 = new Course("Mathématiques");
        prof1.addCourse(math2);

        //add course to student (now we can see all courses for one specific student)
        student1.addCourse(math, french);
        student2.addCourse(science);
        student3.addCourse(math, french, science);

        //add student to course (now we can see the avg of the course)
        math.addStudent(student1, student3);
        french.addStudent(student1, student3);
        science.addStudent(student2, student3);

        //we can see professor like this
        System.out.println(prof1+"\n");

        //we can see student like this
        System.out.println(student3+"\n");

        //we can see course like this
        System.out.println(french+"\n");

        //we can see professor courses
        System.out.println(prof1.getFullName() +" enseigne "+prof1.getCourses(true)+"\n");

        //we can see student courses
        System.out.println(student2.getFullName() +" a comme cours : "+student2.getCourses(true)+"\n");

        //prof can set the grade
        try {
            prof1.setGrade(student3, math, 2.00, "Fantastique.");
            prof1.setGrade(student3, math, 2.00, "Fantastique."); //cannot add math again
            prof3.setGrade(student3, science, 17.12, "BRAVO !!");
            prof2.setGrade(student3, french, 13.00, "Et bien...");
            prof1.setGrade(student1, math, 5.12, "Attention.");
            prof2.setGrade(student1, french, 9.12);
        } catch (Exception e) {
            System.out.println("\nERROR : "+e.getMessage());
        }

        //now we can show report card
        System.out.println("Bulletin de "+student3.getFullName()+"\n"+student3.getReportCard().toString());
        System.out.println("\nBulletin de "+student1.getFullName()+"\n"+student1.getReportCard().toString());

        //error
        try {
            prof2.setGrade(null, null, 9.12);
        } catch (Exception e) {
            System.out.println("\nERROR : "+e.getMessage());
        }

        //error
        try {
            prof2.setGrade(student1, french, -12.03);
        } catch (Exception e) {
            System.out.println("ERROR : "+e.getMessage());
        }

        //student doesn't have this course !
        try {
            prof3.setGrade(student1, science, 17.00, "BRAVO !!");
        } catch (Exception e) {
            System.out.println("ERROR : "+e.getMessage());
        }

        //professor doesn't have this course !
        try {
            prof1.setGrade(student1, science, 4.00, "BRAVO !!");
        } catch (Exception e) {
            System.out.println("ERROR : "+e.getMessage());
        }

        //what is the avg of math ?
        System.out.println("\nLa moyenne de la classe du cours de mathématiques est "+math.average());
    }
}