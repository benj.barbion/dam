<?php

namespace Database\Seeders;

use App\Actions\CreateDefinedMetadata;
use App\Enums\InputType;
use Illuminate\Database\Seeder;

class DefinedMetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(CreateDefinedMetadata $definedMetadata): void
    {
        $definedMetadata->handle([
            'display_name' => 'Auteur',
            'name' => 'auteur',
            'input' => InputType::TEXT,
            'content_types' => [4,6]
        ]);

        $definedMetadata->handle([
            'display_name' => 'Email de contact',
            'name' => 'email',
            'input' => InputType::EMAIL,
            'content_types' => [4,6]
        ]);

        $definedMetadata->handle([
            'display_name' => 'Portfolio',
            'name' => 'portfolio',
            'description' => 'Site web concernant l\'asset.',
            'input' => InputType::URL,
            'content_types' => [4,6]
        ]);
    }
}
