<?php

namespace Database\Factories;

use App\Enums\AssetStatus;

use App\Models\Folder;
use App\Models\MimeType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Folder>
 */
class FolderFactory extends Factory
{
    protected $model = Folder::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => Str::uuid(),
            'name' => $this->faker->word,
            'status' => AssetStatus::PENDING,
            'mime_type_id' => fn () => MimeType::firstOrCreate(['name' => \App\Enums\MimeType::Folder->value])->id
        ];
    }
}
