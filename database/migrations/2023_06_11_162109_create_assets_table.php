<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string('type')->index(); // Column add for STI
            $table->string('name')->index();
            $table->text('description')->nullable();
            $table->string('real_path')->nullable();
            $table->bigInteger('size')->nullable();
            $table->json('embedded_metadata')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->foreignId('parent_id')->nullable()->constrained('assets')->cascadeOnDelete();
            $table->foreignId('mime_type_id')->constrained()->cascadeOnDelete();
            $table->foreignId('user_id')->nullable()->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assets');
    }
};
