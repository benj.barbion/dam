<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta name="application-name" content="{{ config('app.name') }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="{{ asset('icons/favicon-32x32.png') }}" type="image/png">

        <title>{{ config('app.name') }}</title>

        @filamentStyles
        @vite('resources/css/app.css')
    </head>

    <body class="antialiased">
        <div class="flex flex-col min-h-screen">
            <div class="flex-grow">
                <livewire:navigation-menu />

                <div class="py-7 max-w-screen-2xl mx-auto sm:px-6 lg:px-8">
                    {{ $slot }}
                </div>
            </div>

            <footer class="bg-white border-t-2 border-gray-100 text-center p-4 shadow">
                <p>© {{ date('Y') }} Digital Asset. Tous droits réservés.</p>
            </footer>
        </div>

        <livewire:notifications />

        @persist('modal')
            <livewire:modal />
        @endpersist
    </body>

    <script>
        window.UserId = @auth {{ auth()->user()->id }} @else null @endauth
    </script>

    @filamentScripts
    @vite('resources/js/app.js')
</html>
