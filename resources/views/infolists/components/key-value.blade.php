<x-dynamic-component :component="$getEntryWrapperView()" :entry="$entry">
    <div>
        <div class="p-4 border rounded-lg text-sm">
            @forelse($getState() ?? [] as $key => $value)
                <div class="flex justify-between">
                    <span class="text-gray-500">{{ $key }}</span>
                    <span>{{ $value }}</span>
                </div>
            @empty
                Vide
            @endforelse
        </div>
    </div>
</x-dynamic-component>
