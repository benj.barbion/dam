<nav x-data="{ open: false }" class="bg-white border-b border-gray-100 p-2 shadow-md">

    <div class="max-w-screen-2xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <header class="flex items-center">
                    <div class="shrink-0">
                        <a href="{{ route('dashboard') }}" wire:navigate>
                            <img src="{{asset('/icons/logo.svg')}}" alt="logo" class="h-9 w-auto"/>
                        </a>
                    </div>
                </header>
            </div>

            @if(request()->routeIs('dashboard', 'admin.assets', 'files.show', 'livewire.update'))
                @include('partials.navigation-menu.filter')
            @endif

            <div class="hidden md:flex md:items-center md:ml-6 w-1/3">

                <div class="mr-3 grow">
                    @if(request()->routeIs('dashboard', 'admin.assets', 'files.show', 'livewire.update'))
                        <form x-data="{search: ''}" @submit.prevent="$dispatch('search-user', {'data' : { 'global' : search }})">
                            <label>
                                <input x-model="search" type="search" placeholder="Rercherche... (nom, tag, métadonnée, etc.)" class="w-full p-3 h-12 border-2 rounded-lg">
                            </label>
                        </form>
                    @endif
                </div>

                <div class="ml-3 relative">
                    @auth
                        <x-dropdown align="right" width="48">
                            <x-slot name="trigger">
                                <span class="relative inline-flex">
                                    <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition">
                                        <img class="h-12 w-12 rounded-full object-cover"
                                             src="{{ Auth::user()->profile_photo_url }}"
                                             alt="{{ Auth::user()->name }}"/>
                                    </button>

                                    @if(Auth::user()->unreadNotifications->count() > 0)
                                        <span class="absolute top-0 right-0 flex h-3 w-3">
                                            <span class="animate-ping absolute inline-flex h-3 w-3 rounded-full bg-red-400 opacity-75"></span>
                                            <span class="relative inline-flex rounded-full h-3 w-3 bg-red-500"></span>
                                        </span>
                                    @endif
                                </span>
                            </x-slot>

                            <x-slot name="content">
                                <div class="block px-4 py-2 text-xs text-gray-400">
                                    {{ __('Mon compte') }}
                                </div>

                                <x-dropdown-link :href="route('profile.show')" class="cursor-pointer">
                                    {{ __('Profile') }}
                                </x-dropdown-link>

                                <x-dropdown-link @click="$dispatch('modal', {component: 'user-notification'})"
                                                 class="cursor-pointer flex">
                                    {{ __('Notifications') }}

                                    <x-filament::badge
                                            :color="Auth::user()->unreadNotifications->count() > 0 ? 'danger' : 'info'"
                                            class="ml-2">
                                        {{ Auth::user()->unreadNotifications->count() }}
                                    </x-filament::badge>
                                </x-dropdown-link>

                                @canany(['users.update', 'metadata.update', 'files.update'])
                                    <div class="block px-4 py-2 text-xs text-gray-400">
                                        {{ __('Permissions') }}
                                    </div>
                                    @can('files.update')
                                        <x-dropdown-link :href="route('admin.assets')" class="cursor-pointer" wire:navigate>
                                            {{ __('Les assets') }}
                                        </x-dropdown-link>
                                    @endcan
                                    @can('users.update')
                                        <x-dropdown-link :href="route('admin.users')" class="cursor-pointer" wire:navigate>
                                            {{ __('Les utilisateurs') }}
                                        </x-dropdown-link>
                                    @endcan
                                    @can('metadata.update')
                                        <x-dropdown-link :href="route('admin.metadata')" class="cursor-pointer" wire:navigate>
                                            {{ __('Les métadonnées') }}
                                        </x-dropdown-link>
                                    @endcan
                                    <div class="mb-2"></div>
                                @endcanany

                                <div class="border-t border-gray-200"></div>

                                <form method="POST" action="{{ route('logout') }}" x-data>
                                    @csrf
                                    <x-dropdown-link href="{{ route('logout') }}" @click.prevent="$root.submit();">
                                        {{ __('Déconnexion') }}
                                    </x-dropdown-link>
                                </form>
                            </x-slot>
                        </x-dropdown>
                    @else
                        <x-filament::button :href="route('login')" tag="a">
                            Connexion
                        </x-filament::button>
                    @endauth
                </div>
            </div>

            <div class="-mr-2 flex items-center md:hidden">
                <button @click="open = ! open"
                        class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex"
                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M4 6h16M4 12h16M4 18h16"/>
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                    </svg>
                </button>
            </div>
        </div>
    </div>

    @include('partials.navigation-menu.responsive-menu')

</nav>
