<div>
    <div class="mb-3 flex @unless($search) justify-end @else justify-between @endif space-x-2">

        @unless($search === null)
            <x-form.info-button @click="$wire.set('search', null)">
                Réinitialiser
            </x-form.info-button>
        @endif

        <div class="flex space-x-2">
            <x-form.primary-button @click="$dispatch('modal', { component: 'asset.filter' })">
                Filtrer les données
            </x-form.primary-button>
            <div wire:loading.flex>
                <div class="animate-spin rounded-full h-6 w-6 border-t-2 border-b-2 border-blue-600 mt-1"></div>
            </div>
        </div>

    </div>

    <div>{{ $this->table }}</div>
</div>
