<x-modal wire:model="show">
    @if ($view != null)
        <div class="p-3 max-h-[48rem] overflow-y-auto">
            @persist($view.time())
                @livewire($view, $params, key($view.time()))
            @endpersist
        </div>
    @endif
</x-modal>
