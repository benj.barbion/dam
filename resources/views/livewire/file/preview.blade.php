<div>
    <div class="flex justify-center">
        @if(optional($asset)->mimeType)
            @if($component = $this->getComponent())
                <x-dynamic-component :component="$component" :asset="$asset" />
            @else
                <p>Aucune preview disponible pour ce fichier</p>
            @endif
        @endif
    </div>
</div>
