<div class="mt-2" x-data="{ isOpen: $wire.entangle('isOpen') }">
    <button class="flex items-center space-x-2 bg-transparent border-none cursor-pointer">

        <svg @click="isOpen = !isOpen; $wire.loadChildren()" class="w-5 h-5 text-gray-500 transform transition-transform duration-200"
             :class="{'rotate-90': isOpen}" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path>
        </svg>

        <img @click="isOpen = !isOpen; if (isOpen) $wire.loadChildren()" src="{{asset('icons/folder-svgrepo-com.svg')}}" class="w-5 h-5" alt="open" />

        <a @click="isOpen = true; $wire.loadChildren()" href="{{route('files.show', $folder->uuid)}}" class="text-gray-700 text-md" wire:navigate>
            {{ $folder->name }}
        </a>
    </button>

    <ul class="pl-5" x-show="isOpen">
        @foreach ($children as $child)
            <li><livewire:folder.tree :folder="$child" :wire:key="$child->id" /></li>
        @endforeach
    </ul>
</div>
