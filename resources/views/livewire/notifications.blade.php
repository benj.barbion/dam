<div>
    <x-filament::section>
        <x-slot name="heading">
            Liste des différentes notifications
        </x-slot>

        <div class="space-y-2">
            @forelse ($notifications as $notification)
                <div class="border p-4 rounded {{ $loop->last ? '' : 'border-b border-gray-200' }} flex justify-between items-center space-x-4">
                    <span class="text-sm text-gray-600">
                        <strong>{{$notification->data['title']}}</strong> : {{ $notification->data['message'] }}
                    </span>
                    <div>
                        <x-heroicon-o-x-mark
                            class="text-red-600 w-7 h-7 cursor-pointer hover:text-red-400 transition duration-300 ease-in-out"
                            @click="$wire.markAsRead('{{ $notification->id }}')"/>
                    </div>
                </div>
            @empty
                <div class="px-4 py-2 text-sm text-gray-400">
                    Pas de notifications non lues
                </div>
            @endforelse
        </div>
    </x-filament::section>
</div>
