<div>
    @auth
        <form wire:submit="save">
            {{ $this->form }}
            <div class="flex justify-end">
                <x-form.primary-button type="submit" class="mt-5">
                    Lancer la recherche
                </x-form.primary-button>
            </div>
        </form>
    @else
        <x-general.restricted />
    @endauth
</div>
