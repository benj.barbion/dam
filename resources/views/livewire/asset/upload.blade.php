<div class="m-3 mb-1">

    <x-general.errors />

    @cannot('updateStatus', \App\Models\File::class)
        <x-general.alert text="Etant donné votre rôle, vos fichiers doivent être validés. Vous ne pourrez donc les voir que dans vos assets." />
    @endcannot

    @can('create', \App\Models\Folder::class)

        <form class="flex items-center" wire:submit="createFolder">
            <input type="text" placeholder="Nom du dossier" class="grow px-3 h-10 border-2 border-gray-200 rounded-lg mr-2 block" wire:model.blur="name" required>
            <x-form.primary-button type="submit" class="h-10">
                Créer un dossier
            </x-form.primary-button>
        </form>

        <x-general.title text="OU..." class="text-center mt-2" />

    @endcan

    <div
        wire:ignore
        x-data="{
            totalFiles: 0,
            processedFiles: 0,
            initFilepond () {
                const pond = FilePond.create(this.$refs.filepond, {
                    labelIdle: 'Déposez vos fichiers ou <span class=\'filepond--label-action\'> Parcourez </span>',
                    maxFileSize: '200MB',
                    allowImagePreview: false,
                    allowRevert: false,
                    credits: false,
                    onaddfile: () => this.totalFiles++,
                    onprocessfile: (error, file) => {
                        this.processedFiles++
                        if(this.processedFiles === this.totalFiles)
                            @this.endOfUpload()
                    },
                    server: {
                        process: (fieldName, file, metdata, load, error, progress, abort, transfer, options) => @this.upload('upload', file, load, error, progress)
                    }
                });
            }
        }"
        x-init="initFilepond">
        <div>
            <form>
                @csrf
                <input type="file" name="file" x-ref="filepond" multiple>
            </form>
        </div>
    </div>
</div>
