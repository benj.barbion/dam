<div>
    @cannot('updateStatus', $asset)
        <x-general.alert text="Etant donné votre rôle, vos fichiers doivent être validés. Une modification mettra donc votre fichier en attente." />
    @endcannot

    <form wire:submit="save">
        {{ $this->form }}

        <div class="flex justify-end">
            <x-form.primary-button type="submit" class="mt-5">
                Sauver les données
            </x-form.primary-button>
        </div>
    </form>
</div>
