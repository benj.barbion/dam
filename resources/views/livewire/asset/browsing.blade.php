<div x-data="{ grid: false, open: false }" x-init="grid = localStorage.getItem('grid') === 'true' ? true : false">
    <div class="flex justify-between mb-4 order-2 font-bold items-center">

        @auth
            @if($search === null)
                <x-form.primary-button @click="$dispatch('modal', { component: 'asset.upload', params: { uuid : '{{$root->uuid}}'} })">
                    + Ajouter un asset
                </x-form.primary-button>
            @else
                <x-form.info-button @click="$wire.resetFilter">
                    Réinitialiser
                </x-form.info-button>
            @endif
        @else
            <x-filament::button :href="route('register')" tag="a">
                Devenez contributeur !
            </x-filament::button>
        @endauth

        {{ $assets_count }} Assets

        <div class="flex space-x-5">
            <div class="flex space-x-2">
                <button @click="grid=true; localStorage.setItem('grid', true)" :disabled="grid" :class="!grid ? 'opacity-50' : 'opacity-100'" class="hover:opacity-100">
                    <img src="{{asset('/icons/grid-svgrepo-com.svg')}}" class="h-7" alt="grid-view" />
                </button>
                <button @click="grid=false; localStorage.setItem('grid', false)" :disabled="!grid" :class="grid ? 'opacity-50' : 'opacity-100'" class="hover:opacity-100">
                    <img src="{{asset('/icons/list-1540-svgrepo-com.svg')}}" class="h-6" alt="list-view" />
                </button>
            </div>
            <button @click="$dispatch('modal', { component: 'asset.filter' })" class="cursor-pointer opacity-50 hover:opacity-100">
                <img src="{{asset('/icons/filter-svgrepo-com.svg')}}" class="h-6" alt="filter" />
            </button>
        </div>

    </div>

    <div wire:loading.flex class="justify-center items-center min-h-full mt-5">
        <div class="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-blue-600"></div>
    </div>

    @if($files->isEmpty())
        <div class="bg-blue-100 border border-blue-500 text-blue-700 px-4 py-3 rounded relative" role="alert">
            <strong class="font-bold">Aucun résultat !</strong>
            <span class="block sm:inline">Désolé, aucun fichier n'a été trouvé.</span>
        </div>
    @endif

    @unless($files->isEmpty())
        <div wire:loading.remove xmlns:wire="http://www.w3.org/1999/xhtml">
            @include('partials.browsing.files-grid')
            @include('partials.browsing.files-list')
        </div>
    @endif
</div>
