<div>
    <div class="bg-white sm:rounded-lg">
        <div class="flex flex-col lg:justify-between lg:flex-row lg:space-x-14" xmlns:wire="http://www.w3.org/1999/xhtml">
            <div class="w-2/12 hidden lg:flex lg:flex-col">

                <h2 class="text-2xl font-bold text-gray-800 mb-4">Dossiers</h2>
                @persist('file-tree')
                    <livewire:folder.tree :folder="$root" isOpen="true" />
                @endpersist

                @auth
                    <div class="bg-white shadow rounded-lg border-l-4 border-blue-500 cursor-pointer mt-10"
                         @click="$dispatch('search-user', { data : { owner : {{auth()->id()}} }})">
                        <div class="px-4 py-2">
                            <div class="flex items-center">
                                <div>
                                    <dl>
                                        <dt class="text-sm font-medium text-gray-500 truncate">Mes différents assets</dt>
                                        <dd><div class="text-2xl font-semibold text-gray-900">{{ $assets_count }}</div></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                @endauth

                <div class="mt-10">
                    <h2 class="text-2xl font-bold text-gray-800 mb-4">Nuages de tags</h2>
                    <div class="flex flex-wrap">
                        @foreach($tags as $tag)
                            <button
                                @click="$dispatch('search-user', { data : {{json_encode(['tags' => [$tag->name]])}} })"
                                class="bg-blue-600 text-white px-3 py-1 rounded-full text-xs mr-2 mb-2 hover:bg-blue-500 transition-colors duration-300">
                                {{ $tag->name }}
                            </button>
                        @endforeach
                    </div>
                </div>

                <div class="mt-10">
                    <h2 class="text-2xl font-bold text-gray-800 mb-4">Nuages de mots-clés</h2>
                    <div class="flex flex-wrap">
                        @foreach($keywords as $keyword)
                            <button
                                @click="$dispatch('search-user', { data : {{json_encode(['keywords' => [$keyword->name]])}} })"
                                class="bg-blue-600 text-white px-3 py-1 rounded-full text-xs mr-2 mb-2 hover:bg-blue-500 transition-colors duration-300">
                                {{ $keyword->name }}
                            </button>
                        @endforeach
                    </div>
                </div>

            </div>

            <div class="flex-wrap w-12/12 lg:w-10/12" wire:ignore>
                <livewire:asset.browsing :root="$root" />
            </div>
        </div>
    </div>
</div>
