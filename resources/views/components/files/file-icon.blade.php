@props(['class' => 'w-6 h-6'])

<img src="{{ asset('icons/' . $getIconName()) }}" alt="File icon" class="{{ $class }}">
