@props(['asset'])

<img class="w-auto h-auto" src="{{route('files.serve', $asset->uuid)}}" alt="{{$asset->name}}" />
