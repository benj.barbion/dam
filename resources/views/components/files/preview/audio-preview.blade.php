@props(['asset'])

<audio x-data x-ref="audio" x-on:click.away="$refs.audio.pause()" x-on:keydown.escape="$refs.audio.pause()" controls>
    <source src="{{route('files.serve', $asset->uuid)}}" type="{{ $asset->mime_type_name }}">
    Your browser does not support the audio element.
</audio>
