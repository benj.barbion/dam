@props(['asset'])

<video x-data x-ref="video" x-on:click.away="$refs.video.pause()" x-on:keydown.escape="$refs.video.pause()" class="w-full" controls id="{{$asset->id}}">
    <source src="{{ route('files.serve', $asset->uuid) }}" type="video/mp4">
    Your browser does not support the video tag.
</video>
