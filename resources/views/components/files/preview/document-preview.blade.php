@props(['asset'])

<embed src="{{route('files.serve', $asset->uuid)}}" type="{{$asset->mime_type_name}}" width="100%" height="600px" />
