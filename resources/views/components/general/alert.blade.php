@props(['text'])

<div x-data="{ open: true }" x-show="open" class="bg-red-500 text-white px-6 py-4 border-0 rounded relative mb-4">
    <strong class="inline-block">Attention !</strong>
    {{$text}}
    <button @click="open = false" class="absolute bg-transparent text-2xl leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
        <span>×</span>
    </button>
</div>
