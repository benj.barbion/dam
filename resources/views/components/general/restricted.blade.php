@props(['text' => 'aux filtres personnalisés'])

<x-filament::section>
    <x-slot name="heading">Réservé à nos membres.</x-slot>
    <x-filament::link :href="route('register')">Devenez contributeur</x-filament::link> dès aujourd'hui et accédez {{$text}} !
</x-filament::section>
