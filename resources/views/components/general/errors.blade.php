@props(['errors' => collect()])

@if($errors->any())
    <div x-data="{ open: true }" x-show="open" class="bg-red-500 text-white px-6 py-4 border-0 rounded relative mb-4">
        <strong class="inline-block">Oops! Une erreur est survenue.</strong>
        <ul class="list-inside list-disc mt-2">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button @click="open = false" class="absolute bg-transparent text-2xl leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
            <span>×</span>
        </button>
    </div>
@endif
