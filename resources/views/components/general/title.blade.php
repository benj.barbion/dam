@props(['text'])

<h1 {{ $attributes->merge(['class' => 'text-2xl font-bold text-blue-600 mb-3']) }}>
    {{ $text }}
</h1>
