<x-dropdown>
    <x-slot name="trigger">
        •••
    </x-slot>
    <x-slot name="content">
        @unless($child->isFolder)
            <x-dropdown-link href="{{ route('files.download', $child) }}">
                {{ __('Télécharger') }}
            </x-dropdown-link>
        @endunless

        <x-dropdown-link @click="$dispatch('modal', { component: 'asset.show', params: { uuid : '{{$child->uuid}}' } })">
            {{ __('Détails du fichier') }}
        </x-dropdown-link>

        @canany(['update', 'delete'], $child)
            <div class="block px-4 py-2 text-xs text-gray-400">
                {{ __('Gérer le fichier') }}
            </div>

            <div class="border-t border-gray-200"></div>

            @can('update', $child)
                <x-dropdown-link @click="$dispatch('modal', { component: 'asset.update', params: { uuid : '{{$child->uuid}}' } })">
                    {{ __('Modifier') }}
                </x-dropdown-link>
            @endcan

            @can('delete', $child)
                <x-dropdown-link @click="$dispatch('swal:confirm', { method: 'delete-asset', params: { asset : '{{$child->uuid}}' } })">
                    {{ __('Supprimer') }}
                </x-dropdown-link>
            @endcan
        @endcanany
    </x-slot>
</x-dropdown>
