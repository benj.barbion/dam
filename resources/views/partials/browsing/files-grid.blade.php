<div x-show="grid" style="display: none">
    <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6 gap-4">

        @foreach($files as $child)
            <div class="bg-white border border-gray-200 rounded-lg shadow flex flex-col">

                <div class="flex justify-between mx-1 p-1 items-center space-x-4 cursor-pointer">
                    <div class="truncate">
                        <x-file-icon :mimeType="$child->mime_type_name" class="w-4 h-4 inline mr-1 -mt-1" />
                        <a
                            @unless($child->isFolder)
                                @click="$dispatch('modal', { component : 'file.preview', params: { uuid : '{{$child->uuid}}' } })"
                            @else
                                @click="$dispatch('modify-folder.{{$child->uuid}}')"
                                href="{{route('files.show', $child->uuid)}}"
                                wire:navigate
                            @endunless
                        >
                            {{$child->name}}
                        </a>
                    </div>

                    @include('partials.browsing.action')
                </div>


                @if($child->isFolder)
                    <a class="cursor-pointer flex flex-grow justify-center items-center h-24 sm:h-40 md:h-40 lg:h-44" href="{{route('files.show', $child->uuid)}}"
                       @click="$dispatch('modify-folder.{{$child->uuid}}')" wire:navigate>
                        <x-file-icon :mimeType="$child->mime_type_name" class="w-1/3" />
                    </a>
                @else
                    <div @click="$dispatch('modal', { component : 'file.preview', params: { uuid : '{{$child->uuid}}' } })" class="cursor-pointer flex flex-grow justify-center items-center h-24 sm:h-40 md:h-40 lg:h-44">
                        @if($child->thumbnail)
                            <img class="rounded-md object-cover h-full w-full" src="{{ route('files.thumbnail', $child->uuid) }}" alt="{{ $child->title }}" />
                        @else
                            <x-file-icon :mimeType="$child->mime_type_name" class="w-1/3" />
                        @endif
                    </div>
                @endif

                @if($child->status != \App\Enums\AssetStatus::APPROVED)
                    <div class="flex justify-center items-center grow font-bold text-red-400">{{$child->status->getLabel()}}</div>
                @endif
            </div>
        @endforeach

    </div>
    <div class="py-4">{{ $files->links() }}</div>
</div>
