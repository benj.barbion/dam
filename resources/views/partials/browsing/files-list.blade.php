<div x-show="!grid" style="display: none;" class="mt-1">
    <div class="overflow-auto rounded-lg">

        <table class="w-full border border-gray-200 table-auto">
            <thead class="bg-gray-50">
                <tr>
                    <th class="text-left px-3 py-3 font-semibold">Nom du fichier</th>
                    <th class="text-left px-3 py-3 font-semibold">Taille</th>
                    <th class="text-left px-3 py-3 font-semibold">Utilisateur</th>
                    <th class="text-left px-3 py-3 font-semibold">Publié le</th>
                    <th class="px-3 py-3 text-right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($files as $child)

                    <tr class="border border-gray-200 even:bg-gray-50 text-sm">
                        <td class="p-3 flex items-center">
                            @if($child->thumbnail)
                                <img class="w-6 h-6 rounded-full object-cover" src="{{ route('files.thumbnail', $child->uuid) }}" alt="{{ $child->title }}" />
                            @else
                                <x-file-icon :mimeType="$child->mime_type_name" />
                            @endif

                            @if ($child->isFolder)
                                <a @click="$dispatch('modify-folder.{{$child->uuid}}')"
                                   href="{{route('files.show', $child->uuid)}}" class="p-2" wire:navigate>{{$child->name}}</a>
                            @else
                                <button @click="$dispatch('modal', { component: 'file.preview', params: { uuid: '{{$child->uuid}}' }})" class="p-2 text-left">
                                    {{$child->name}}

                                    @if($child->status != \App\Enums\AssetStatus::APPROVED)
                                        <span class="font-bold">({{$child->status->getLabel()}})</span>
                                    @endif
                                </button>
                            @endif
                        </td>

                        <td class="p-3">
                            @if ($child->isFolder)
                                &mdash;
                            @else
                                {{$child->size}}
                            @endif
                        </td>

                        <td class="p-3">
                            @if($child->user)
                                {{$child->user->name}}#{{$child->user->id}}
                            @else
                                &mdash;
                            @endif
                        </td>
                        <td class="p-3">{{$child->created_at}}</td>

                        <td class="p-3 cursor-pointer">
                            @include('partials.browsing.action')
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="py-4">{{ $files->links() }}</div>
</div>
