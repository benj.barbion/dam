<div class="hidden space-x-4 lg:flex md:-my-px md:ml-10 items-center cursor-pointer">
    <div
        @click="$dispatch('search-user', { 'data' : {{json_encode(['content_types' => [\App\Enums\ContentType::Video]])}} })"
        class="transition duration-300 ease-in-out hover:bg-gray-200 rounded-full h-10 w-10 flex items-center justify-center overflow-hidden bg-gray-50 border-gray-400 border-2">
        <img src="{{asset('/icons/video-document-svgrepo-com.svg')}}" class="h-5 object-cover" alt=""/>
    </div>
    <div
        @click="$dispatch('search-user', { 'data' : {{json_encode(['content_types' => [\App\Enums\ContentType::Audio]])}} })"
        class="transition duration-300 ease-in-out hover:bg-gray-200 rounded-full h-10 w-10 flex items-center justify-center overflow-hidden bg-gray-50 border-gray-400 border-2">
        <img src="{{asset('/icons/audio-document-svgrepo-com.svg')}}" class="h-5 object-cover" alt=""/>
    </div>
    <div
        @click="$dispatch('search-user', { 'data' : {{json_encode(['content_types' => [\App\Enums\ContentType::Image]])}} })"
        class="transition duration-300 ease-in-out hover:bg-gray-200 rounded-full h-10 w-10 flex items-center justify-center overflow-hidden bg-gray-50 border-gray-400 border-2">
        <img src="{{asset('/icons/image-document-svgrepo-com.svg')}}" class="h-5 object-cover" alt=""/>
    </div>
    <div
        @click="$dispatch('search-user', { 'data' : {{json_encode(['content_types' => [\App\Enums\ContentType::Text]])}} })"
        class="transition duration-300 ease-in-out hover:bg-gray-200 rounded-full h-10 w-10 flex items-center justify-center overflow-hidden bg-gray-50 border-gray-400 border-2">
        <img src="{{asset('/icons/txt-document-svgrepo-com.svg')}}" class="h-5 object-cover" alt=""/>
    </div>
    <div
        @click="$dispatch('search-user', { 'data' : {{json_encode(['content_types' => [\App\Enums\ContentType::Application]])}} })"
        class="transition duration-300 ease-in-out hover:bg-gray-200 rounded-full h-10 w-10 flex items-center justify-center overflow-hidden bg-gray-50 border-gray-400 border-2">
        <img src="{{asset('/icons/unknown-document-svgrepo-com.svg')}}" class="h-5 object-cover" alt=""/>
    </div>
    <div
        @click="$dispatch('search-user', { 'data' : {{json_encode(['type' => 'Dossier'])}} })"
        class="transition duration-300 ease-in-out hover:bg-gray-200 rounded-full h-10 w-10 flex items-center justify-center overflow-hidden bg-gray-50 border-gray-400 border-2">
        <img src="{{asset('/icons/folder-svgrepo-com.svg')}}" class="h-5 object-cover" alt=""/>
    </div>
</div>
