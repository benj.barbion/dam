<div :class="{'block': open, 'hidden': ! open}" class="hidden md:hidden">

    <form x-data="{search: ''}"
          @submit.prevent="$dispatch('search-user', {'data' : { 'global' : search }})">
        <label>
            <input x-model="search" type="search"
                   placeholder="Rercherche... (nom, tag, métadonnée, etc.)"
                   class="w-full p-3 h-12 border-2 rounded-lg">
        </label>
    </form>


    <div class="pt-4 pb-1 border-t border-gray-200">
        @auth
            <div class="flex items-center px-4">
                <div class="shrink-0 mr-3">
                    <img class="h-10 w-10 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}"/>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <x-responsive-nav-link href="{{ route('profile.show') }}" :active="request()->routeIs('profile.show')">
                    {{ __('Profile') }}
                </x-responsive-nav-link>

                <x-responsive-nav-link @click="$dispatch('modal', {component: 'user-notification'})"
                                 class="cursor-pointer flex">
                    {{ __('Notifications') }}

                    <x-filament::badge
                        :color="Auth::user()->unreadNotifications->count() > 0 ? 'danger' : 'info'"
                        class="ml-2">
                        {{ Auth::user()->unreadNotifications->count() }}
                    </x-filament::badge>
                </x-responsive-nav-link>

                @canany(['users.update', 'metadata.update', 'files.update'])
                    <div class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Permissions') }}
                    </div>
                    @can('files.update')
                        <x-responsive-nav-link :href="route('admin.assets')" class="cursor-pointer" wire:navigate :active="request()->routeIs('admin.assets')">
                            {{ __('Gérer les assets') }}
                        </x-responsive-nav-link>
                    @endcan
                    @can('users.update')
                        <x-responsive-nav-link :href="route('admin.users')" class="cursor-pointer" wire:navigate :active="request()->routeIs('admin.users')">
                            {{ __('Gérer les utilisateurs') }}
                        </x-responsive-nav-link>
                    @endcan
                    @can('metadata.update')
                        <x-responsive-nav-link :href="route('admin.metadata')" class="cursor-pointer" wire:navigate :active="request()->routeIs('admin.metadata')">
                            {{ __('Gérer les métadonnées') }}
                        </x-responsive-nav-link>
                    @endcan
                    <div class="mb-2"></div>
                @endcanany

                <div class="border-t border-gray-200"></div>

                <form method="POST" action="{{ route('logout') }}" x-data>
                    @csrf
                    <x-responsive-nav-link href="{{ route('logout') }}" @click.prevent="$root.submit();">
                        {{ __('Déconnexion') }}
                    </x-responsive-nav-link>
                </form>


            </div>
    </div>
    @endauth
</div>
