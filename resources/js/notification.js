const SwalModal = (icon, title, html)=> {
    Swal.fire({
        icon,
        title,
        html
    })
}

const SwalConfirm = (icon = 'warning', title = 'Certain de votre décision ?', html = 'Cette action est irréversible!',
                     confirmButtonText = 'Oui, supprimer!', method, params, callback) => {
    Swal.fire({
        icon,
        title,
        html,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText,
        cancelButtonText: 'Annuler',
        reverseButtons: true,
    }).then(result => {
        if (result.value)
            return Livewire.dispatch(method, params)
        if (callback)
            return Livewire.dispatch(callback)
    })
}

const SwalAlert = (title = 'Action effectuée avec succès.', icon = 'success', timeout = 7000) => {
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: timeout,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        },
    })

    Toast.fire({
        icon,
        title
    })
}

document.addEventListener('livewire:navigated', () => {

})

document.addEventListener('DOMContentLoaded', () => {

    document.addEventListener('file:update', event => SwalAlert(event.detail.title));

    document.addEventListener('swal:alert', event => SwalAlert(event.detail.title, event.detail.icon));

    document.addEventListener('swal:confirm', function (data)
    {
        SwalConfirm(data.detail.icon, data.detail.title, data.detail.text, data.detail.confirmText, data.detail.method, data.detail.params, data.detail.callback)
    });

    if (window.UserId != null)
    {
        Echo
            .private('App.Models.User.' + window.UserId)
            .notification(function () {
                Livewire.dispatch('notificationRead');
                Livewire.dispatch('refresh-navigation-menu');
            });
    }
});
