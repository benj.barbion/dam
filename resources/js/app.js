import swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css'

window.Swal = swal;

import './notification.js'

import * as FilePond from 'filepond';
window.FilePond = FilePond;

import 'filepond/dist/filepond.min.css';

import Echo from 'laravel-echo';
import Pusher from 'pusher-js';

window.Pusher = Pusher;

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: import.meta.env.VITE_PUSHER_APP_KEY,
    cluster: import.meta.env.VITE_PUSHER_APP_CLUSTER,
    forceTLS: true
});
